<?php

namespace App\Tests\Api\Controller;

use App\Entity\Exchanger;
use App\Entity\User;
use App\Tests\ApiTestCase;
use GuzzleHttp\Exception\ClientException;

class ExchangerControllerTest extends ApiTestCase
{
    public function testGETindex()
    {
        $response = $this->client->get('/api/exchangers/1/10', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETindexAvailableForAdmin()
    {
        $this->testPOSTcreate();

        $response = $this->client->get('/api/exchangers/available/?admin=true', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // check if exchanger has admin
        $exchanger = $this->readResponseProperty($response, 'data')[0];
        $this->assertNotEquals(true, property_exists($exchanger,'admin'));
    }

    public function testGETindexAvailableForOperator()
    {
        $this->testPOSTcreate();

        $response = $this->client->get('/api/exchangers/available/?admin=false', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // check if exchanger has operator
        $exchanger = $this->readResponseProperty($response, 'data')[0];
        $this->assertNotEquals(true, property_exists($exchanger,'operator'));
    }

    public function testGETshow()
    {
        $id = $this->getEntity(new Exchanger())->getId();
        $response = $this->client->get("/api/exchanger/{$id}", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCheckIsTransactionAvailable()
    {
        $id = $this->getEntity(new Exchanger())->getId();
        $response = $this->client->get("/api/exchanger/{$id}/transaction/available", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCheckIsTransactionNotAvailable()
    {
        $this->testPOSTcreate();
        $id = $this->getEntity(new Exchanger(), 4)->getId();
        try {
            $this->client->get("/api/exchanger/{$id}/transaction/available", [
                'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }
        $this->assertEquals(400, $code);
    }

    public function testPOSTcreate()
    {
        $response = $this->client->post('/api/exchanger', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'country' => $this->faker->country,
                'city' => $this->faker->city,
                'street' => $this->faker->streetAddress,
                'phone' => $this->faker->phoneNumber,
                'ip' => $this->faker->ipv4,
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPUTupdate()
    {
        $exchanger = $this->getEntity(new Exchanger());
        $response = $this->client->put("/api/exchanger/{$exchanger->getId()}/update", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'country' => $this->faker->country,
                'city' => $this->faker->city,
                'street' => $this->faker->streetAddress,
                'phone' => $this->faker->phoneNumber,
                'ip' => $this->faker->ipv4,
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertNotEquals(
            $this->readResponseProperty($response, 'data')[0]->street,
            $exchanger->getStreet()
        );
    }

    public function testDELETEdisable()
    {
        $exchanger = $this->getEntity(new Exchanger(), 2);
        $response = $this->client->delete("/api/exchanger/{$exchanger->getId()}/disable", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // operator should not login if his exchanger disabled
        $user = $exchanger->getOperator();
        $response = $this->client->post('/api/login', [
            'form_params' => [
                'username' => $user->getUsername(),
                'password' => self::TEST_PASS
            ]
        ]);
        $code = json_decode($response->getBody()->getContents())->code;
        $this->assertEquals(403, $code);
    }
}
