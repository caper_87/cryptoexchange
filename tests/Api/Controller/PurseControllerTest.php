<?php

namespace App\Tests\Api\Controller;

use App\Entity\Exchanger;
use App\Entity\Purse;
use App\Entity\User;
use App\Tests\ApiTestCase;
use GuzzleHttp\Exception\ClientException;

class PurseControllerTest extends ApiTestCase
{
    public function testGETindex()
    {
        $response = $this->client->get('/api/purses', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETshow()
    {
        $id = $this->getEntity(new Purse())->getId();
        $response = $this->client->get('/api/purse/' . $id, [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPOSTcreate()
    {
        $response = $this->client->get('/api/currencies/available', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $availableCurrency = $this->readResponseProperty($response, 'data')[0];

        $response = $this->client->post('/api/purse', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'token' => $this->faker->sha1,
                'currency' => $availableCurrency->id,
                'password' => '123321q'
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

//    public function testPUTupdate()
//    {
//        $purse = $this->getEntity(new Purse());
//        $response = $this->client->put("/api/purse/{$purse->getId()}/update", [
//            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
//            'form_params' => [
//                'token' => $this->faker->sha1,
//            ]
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//
//        $this->assertNotEquals(
//            $this->readResponseProperty($response, 'data')[0]->token,
//            $purse->getToken()
//        );
//    }

    public function testDELETEdisable()
    {
        $purse = $this->getEntity(new Purse());
        $response = $this->client->delete("/api/purse/{$purse->getId()}/disable", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
