<?php

namespace App\Tests\Api\Controller;

use App\Entity\User;
use App\Tests\ApiTestCase;
use GuzzleHttp\Exception\ClientException;

class UserControllerTest extends ApiTestCase
{
    public function testGETindex()
    {
        $response = $this->client->get('/api/users/1/10', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETindexAdmin()
    {
        $response = $this->client->get('/api/users/admin/1/10', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETindexOperator()
    {
        $response = $this->client->get('/api/users/operator/1/10', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETshow()
    {
        $response = $this->client->get('/api/user/' . $this->getUser(User::ROLE_ADMIN)->getId(), [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETindexAdminErrorForWrongRoles()
    {
        try {
            $this->client->get('/api/users/admin/1/10', [
                'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_ADMIN)],
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }

        $this->assertEquals(403, $code);

        try {
            $this->client->get('/api/users/admin/1/10', [
                'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_OPERATOR)],
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }

        $this->assertEquals(403, $code);
    }

    public function testGETindexOperatorErrorForWrongRoles()
    {
        try {
            $this->client->get('/api/users/operator/1/10', [
                'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_ADMIN)],
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }

        $this->assertEquals(403, $code);

        try {
            $this->client->get('/api/users/operator/1/10', [
                'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_OPERATOR)],
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }

        $this->assertEquals(403, $code);
    }

    public function testGETshowErrorForWrongRoles()
    {
        try {
            $id = $this->getUser(User::ROLE_OPERATOR)->getId();
            $this->client->get('/api/user/' . $id, [
                'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_ADMIN)],
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }
        $this->assertEquals(403, $code);

        try {
            $id = $this->getUser(User::ROLE_OPERATOR)->getId();
            $this->client->get('/api/user/' . $id, [
                'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_OPERATOR)],
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }
        $this->assertEquals(403, $code);
    }

    public function testPOSTcreateAdmin()
    {
        $response = $this->client->post('/api/user/admin', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'username' => $this->faker->userName,
                'password' => self::TEST_PASS,
                'phone' => $this->faker->phoneNumber,
                'firstName' => $this->faker->userName,
                'lastName' => $this->faker->userName,
                'exchangers' => '[]',
                'permissions' => '[]',
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPOSTcreateOperator()
    {
        $response = $this->client->post('/api/user/operator', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'username' => $this->faker->userName,
                'password' => self::TEST_PASS,
                'phone' => $this->faker->phoneNumber,
                'firstName' => $this->faker->userName,
                'lastName' => $this->faker->userName,
                'exchangers' => '[]',
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPUTupdate()
    {
        $user = $this->getUser(User::ROLE_ADMIN);
        $newName = $this->faker->userName;
        $response = $this->client->put("/api/user/{$user->getId()}/update", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'password' => self::TEST_PASS,
                'phone' => $this->faker->phoneNumber,
                'firstName' => $newName,
                'lastName' => $this->faker->userName,
                'exchangers' => '[]',
                'permissions' => '[]',
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertNotEquals(
            $this->readResponseProperty($response, 'data')[0]->firstName,
            $user->getFirstName()
        );
    }

    public function testDELETEdisableUser()
    {
        $user = $this->getUser(User::ROLE_OPERATOR);
        $response = $this->client->delete("/api/user/{$user->getId()}/disable", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // login error
        try {
            $this->client->post('/api/login', [
                'form_params' => [
                    'username' => $user->getUsername(),
                    'password' => self::TEST_PASS
                ]
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }
        $this->assertEquals(403, $code);
    }

    public function testDELETEenableUser()
    {
        $user = $this->getUser(User::ROLE_ADMIN);
        $response = $this->client->delete("/api/user/{$user->getId()}/disable", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        // disable
        $this->assertEquals(200, $response->getStatusCode());

        // login error
        try {
            $this->client->post('/api/login', [
                'form_params' => [
                    'username' => $user->getUsername(),
                    'password' => self::TEST_PASS
                ]
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }
        $this->assertEquals(403, $code);

        // enable
        $response = $this->client->delete("/api/user/{$user->getId()}/disable", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // login success
        $this->client->post('/api/login', [
            'form_params' => [
                'username' => $user->getUsername(),
                'password' => self::TEST_PASS
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
