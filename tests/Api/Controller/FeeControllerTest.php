<?php

namespace App\Tests\Api\Controller;

use App\Entity\Currency;
use App\Entity\Fee;
use App\Tests\ApiTestCase;
use GuzzleHttp\Exception\ClientException;

class FeeControllerTest extends ApiTestCase
{
    public function testGETindex()
    {
        $response = $this->client->get('/api/fees', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETshow()
    {
        $fee = $this->getEntity(new Fee());
        $response = $this->client->get("/api/fee/{$fee->getId()}", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETgetStaticFee()
    {
        $response = $this->client->get('/api/fee/static/', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // check if currency has purse
        $fee = $this->readResponseProperty($response, 'data');
        $this->assertEquals(true, property_exists($fee,'staticFee'));
    }

    public function testPOSTsetStaticFee()
    {
        $response = $this->client->post('/api/fee/static', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'staticFee' => 10,
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPUTupdate()
    {
        $json = "[ { \"id\": 1, \"currencyIn\": \"BTC\", \"currencyOut\": \"USD\", \"percent\": 0.1, \"direction\": 1 }, { \"id\": 2, \"currencyIn\": \"USD\", \"currencyOut\": \"BTC\", \"percent\": 0.1, \"direction\": 2 } ]";

        $response = $this->client->put("/api/fee/update", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'body' => $json
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPOSTcalculateCourse()
    {
        $json = "[ { \"course\": 10.89, \"code\": \"BTC\", \"currency\": 1, \"type\": 1 }, { \"course\": 10.89, \"code\": \"BTC\", \"currency\": 1, \"type\": 2 }, { \"course\": 10.89, \"code\": \"DSH\", \"currency\": 2, \"type\": 1 }, { \"course\": 10.89, \"code\": \"DSH\", \"currency\": 2, \"type\": 2 } ]";
        $response = $this->client->post('/api/fee/course/calculate', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'body' => $json
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $fee = $this->readResponseProperty($response, 'data')[0];
        $this->assertEquals(true, property_exists($fee,'course'));
    }

    public function testPOSTcalculateAmount()
    {
        $response = $this->client->post('/api/fee/calculate', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'amount' => 1,
                'course' => 7800,
                'type' => 1,
                'currency' => 1,
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $fee = $this->readResponseProperty($response, 'data');
        $this->assertEquals(true, property_exists($fee,'amount'));
    }
}
