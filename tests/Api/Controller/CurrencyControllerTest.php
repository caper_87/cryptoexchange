<?php

namespace App\Tests\Api\Controller;

use App\Entity\Currency;
use App\Tests\ApiTestCase;
use GuzzleHttp\Exception\ClientException;

class CurrencyControllerTest extends ApiTestCase
{
    public function testGETindex()
    {
        $response = $this->client->get('/api/currencies', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETindexAvailable()
    {
        $response = $this->client->get('/api/currencies/available', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // check if currency has purse
        $currency = $this->readResponseProperty($response, 'data')[0];
        $this->assertNotEquals(true, property_exists($currency,'purses'));
    }

    public function testGETshow()
    {
        $currency = $this->getEntity(new Currency());
        $response = $this->client->get("/api/currency/{$currency->getId()}", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPOSTcreate()
    {
        $response = $this->client->post('/api/currency', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'title' => $this->faker->country,
                'code' => $this->faker->currencyCode,
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPUTupdate()
    {
        $currency = $this->getEntity(new Currency());
        $response = $this->client->put("/api/currency/{$currency->getId()}/update", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'title' => $this->faker->country,
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertNotEquals(
            $this->readResponseProperty($response, 'data')[0]->title,
            $currency->getTitle()
        );
    }

//    public function testDELETEdelete()
//    {
//        $currency = $this->getEntity(new Currency());
//        $response = $this->client->delete("/api/currency/{$currency->getId()}/delete", [
//            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
//        ]);
//        $this->assertEquals(200, $response->getStatusCode());
//    }
}
