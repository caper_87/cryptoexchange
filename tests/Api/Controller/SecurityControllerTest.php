<?php

namespace App\Tests\Api\Controller;

use App\Entity\User;
use App\Tests\ApiTestCase;
use GuzzleHttp\Exception\ClientException;

class SecurityControllerTest extends ApiTestCase
{
    public function testPOSTLogin()
    {
        $response = $this->client->post('/api/login', [
            'form_params' => [
                'username' => $this->getUser(User::ROLE_ADMIN)->getUsername(),
                'password' => self::TEST_PASS
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testTokenInvalidCredentials()
    {
        try {
            $this->client->post('/api/login', [
                'form_params' => [
                    'username' => 'test123',
                    'password' => '123321'
                ]
            ]);
        } catch (ClientException $e) {
            $code = json_decode($e->getResponse()->getBody()->getContents())->code;
        }

        $this->assertEquals(403, $code);
    }

    public function testRequiresAuthenticationFail()
    {
        try {
            $this->client->get('/api/users/1/10', [
                'headers' => ['Authorization' => 'Bearer 123'],
            ]);
        } catch (ClientException $e) {
            $code = $this->readResponseProperty($e->getResponse(), 'code');
        }

        $this->assertEquals(401, $code);
    }

    public function testRequiresAuthenticationSuccess()
    {
        $response = $this->client->get('/api/users/1/10', [
            'headers' => ['Authorization' => 'Bearer '.$this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLogout()
    {
        $token = $this->tokenProvider();
        $response = $this->client->delete('/api/logout', [
            'headers' => ['Authorization' => 'Bearer '.$token],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        /**
         * Test token invalidate
         */
        try {
            $this->client->get('/api/users/1/10', [
                'headers' => ['Authorization' => 'Bearer 123'],
            ]);
        } catch (ClientException $e) {
            $code = $this->readResponseProperty($e->getResponse(), 'code');
        }

        $this->assertEquals(401, $code);
    }
}
