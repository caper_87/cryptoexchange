<?php

namespace App\Tests\Api\Controller;

use App\Entity\Currency;
use App\Entity\Transaction;
use App\Entity\User;
use App\Tests\ApiTestCase;

class TransactionControllerTest extends ApiTestCase
{
    public function testGETindex()
    {
        $response = $this->client->get('/api/transactions/1/10', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETshow()
    {
        $id = $this->getEntity(new Transaction())->getId();
        $response = $this->client->get('/api/transaction/' . $id, [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_OPERATOR)],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPOSTcreate()
    {
        $currency = $this->getEntity(new Currency(), rand(1,5));
        $response = $this->client->post('/api/transaction', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_OPERATOR)],
            'form_params' => [
                'income' => $this->faker->randomFloat(2, 10, 100),
                'outcome' => $this->faker->randomFloat(2, 10, 100),
                'type' => 1,
                'course' => $this->faker->randomFloat(2, 1, 10),
                'currency' => $currency->getId(),
                'purse' => $this->faker->sha1,
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPUTupdateStatus()
    {
        $transaction = $this->getEntity(new Transaction());
        $newStatus = Transaction::STATUS_DONE;
        $response = $this->client->put("/api/transaction/{$transaction->getId()}/status", [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider(User::ROLE_ADMIN)],
            'form_params' => [
                'status' => $newStatus,
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertNotEquals(
            $this->readResponseProperty($response, 'data')[0]->status,
            $transaction->getStatus()
        );
    }
}
