<?php

namespace App\Tests;

use App\DataFixtures\LoadUserData;
use App\Entity\Exchanger;
use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Mapping\Entity;
use Faker\Factory;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Bundle\FrameworkBundle\Console\Application;

class ApiTestCase extends KernelTestCase
{
    const TEST_EMAIL = 'test@mail.com';
    const TEST_PASS = '123321q';

    private static $staticClient;

    private static $staticContainer;

    /**
     * @var Client
     */
    protected $client;

    protected static $application;

    protected $faker;

    protected $token;

    protected $container;

    public $user;

    protected function setUp()
    {
        $this->client = self::$staticClient;
        $this->container = self::$staticContainer;
        $this->faker = Factory::create();
    }

    protected function tearDown()
    {
    }

    public static function setUpBeforeClass()
    {
        self::bootKernel();
        self::$staticContainer = self::$kernel->getContainer();
        self::$staticClient = new Client([
            'base_uri' => 'http://127.0.0.1:8000',
            'defaults' => [
                'exceptions' => false
            ]
        ]);
        self::runCommand('doctrine:database:create --if-not-exists');
        self::runCommand('doctrine:migrations:migrate');
        self::runCommand('doctrine:fixtures:load');
    }

    public static function tearDownAfterClass()
    {
        self::runCommand('doctrine:database:drop --force');
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            self::$application = new Application(self::$kernel);
            self::$application->setAutoExit(false);
        }
        return self::$application;
    }

    protected function purgeDatabase()
    {
        $purger = new ORMPurger($this->getService('doctrine.orm.default_entity_manager'));
        $purger->purge();
    }

    /**
     * @param $role
     * @return User
     */
    public function getUser($role)
    {
        /** Doctrine\ORM\EntityManager $em */
        $em = $this->getService('doctrine.orm.default_entity_manager');
        return $em->getRepository(User::class)->findOneByRole($role);
    }

    /**
     * @param $entity
     * @param int $id
     * @return object
     */
    public function getEntity($entity, $id = 1)
    {
        /** Doctrine\ORM\EntityManager $em */
        $em = $this->getService('doctrine.orm.default_entity_manager');
        return $em->getRepository(get_class($entity))->find($id);
    }

    public function createAdmin($username = null)
    {
        $response = $this->client->post('/api/user/admin', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'username' => $username ?? 'admin',
                'password' => self::TEST_PASS,
                'phone' => $this->faker->phoneNumber,
                'firstName' => $this->faker->userName,
                'lastName' => $this->faker->userName,
                'exchangers' => '[]',
                'permissions' => '[]',
            ]
        ]);
        return $this->readResponseProperty($response, 'data')[0];
    }

    public function createOperator($username = null)
    {
        $response = $this->client->post('/api/user/operator', [
            'headers' => ['Authorization' => 'Bearer ' . $this->tokenProvider()],
            'form_params' => [
                'username' => $username ?? 'operator',
                'password' => self::TEST_PASS,
                'phone' => $this->faker->phoneNumber,
                'firstName' => $this->faker->userName,
                'lastName' => $this->faker->userName,
                'exchangers' => '[]',
            ]
        ]);
        return $this->readResponseProperty($response, 'data')->user;
    }

    protected function getService($id)
    {
        return $this->container->get($id);
    }

    public function tokenProvider($role = null)
    {
        $role = $role ?? User::ROLE_SUPER_ADMIN;
        $user = $this->getUser($role);
        $response = $this->client->post('/api/login', [
            'form_params' => [
                'username' => $user->getUsername(),
                'password' => self::TEST_PASS
            ]
        ]);
        return $this->readResponseProperty($response, 'data')->token;
    }

    protected function readResponseProperty($response, $property)
    {
        $content = json_decode($response->getBody()->getContents());
        try {
            return $content->$property;
        } catch (AccessException $e) {
            // it could be a stdClass or an array of stdClass
            $values = is_array($content) ? $content : get_object_vars($content);
            throw new AccessException(sprintf(
                'Error reading property "%s" from available keys (%s)',
                $property,
                implode(', ', array_keys($values))
            ), 0, $e);
        }
    }
}