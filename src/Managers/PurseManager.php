<?php
namespace App\Managers;

use App\Entity\Currency;
use App\Entity\Purse;
use App\Entity\Transaction;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Form;

class PurseManager extends BaseManager
{
    public function __construct(ObjectManager $em)
    {
        parent::__construct($em);
    }

    public function createFromForm(Form $form)
    {
        /** @var Purse $purse */
        $purse = $form->getData();
        $purse->setEnabled(true);
        $this->save($purse);
        return $purse;
    }

    /**
     * @param Purse $purse
     * @return bool
     */
    public function checkOpenTransactions(Purse $purse)
    {
        $transactions = $purse->getTransactions();
        if (!$transactions->isEmpty()) {
            foreach ($transactions as $transaction) {
                if ($transaction->getStatus() == Transaction::STATUS_NEW) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param Purse $purse
     * @return array
     */
    public function getOpenTransactions(Purse $purse)
    {
        $res = [];
        $transactions = $purse->getTransactions();
        if (!$transactions->isEmpty()) {
            foreach ($transactions as $transaction) {
                if ($transaction->getStatus() == Transaction::STATUS_NEW) {
                    $res[] = $transaction->getId();
                }
            }
        }
        return $res;
    }

    /**
     * @param Purse $purse
     */
    public function disable(Purse $purse)
    {
        $purse->setEnabled(!$purse->getEnabled());
        $this->save($purse);
    }

    /**
     * @param $currencyId
     * @return bool
     */
    public function isCurrencyNotAvailable($currencyId)
    {
        return $this->isCurrencyInUse($currencyId) || $this->isCurrencyDisabled($currencyId);
    }

    /**
     * @param $currencyId
     * @return bool
     */
    public function isCurrencyInUse($currencyId)
    {
        return (boolean) $this->getRepository(new Purse())->findBy([
            'currency' => $currencyId,
            'enabled' => true
        ]);
    }

    /**
     * @param $currencyId
     * @return mixed
     */
    public function isCurrencyDisabled($currencyId)
    {
        return !$this->getRepository(new Currency())->find($currencyId)->getEnabled();
    }
}