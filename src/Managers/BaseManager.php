<?php
namespace App\Managers;

use App\Entity\Currency;
use App\Entity\Purse;
use App\Utils\JsonValidator;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BaseManager
{
    private $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function save($obj)
    {
        $this->em->persist($obj);
        $this->em->flush();
    }

    public function delete($obj)
    {
        $this->em->remove($obj);
        $this->em->flush();
    }

    public function getRepository($entity)
    {
        return $this->em->getRepository(get_class($entity));
    }

    public function jsonValidate($modelData, $schemaName)
    {
        $validator = new JsonValidator();
        if (!$validator->validate($modelData, $schemaName)) {
            throw new BadRequestHttpException("Невалидные данные в Json");
        }
    }
}