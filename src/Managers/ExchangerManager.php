<?php
namespace App\Managers;

use App\Entity\Exchanger;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Form;

class ExchangerManager extends BaseManager
{
    public function __construct(ObjectManager $em)
    {
        parent::__construct($em);
    }

    public function saveFromForm(Form $form)
    {
        /** @var Exchanger $exchanger */
        $exchanger = $form->getData();
        $exchanger->setEnabled(true);
        $this->save($exchanger);
        return $exchanger;
    }

    public function disable(Exchanger $exchanger)
    {
        $exchanger->setEnabled(!$exchanger->getEnabled());
        if ($operator = $exchanger->getOperator()) {
            $operator->setHash('You going logout ;)');
            $this->save($operator);
        }
        $this->save($exchanger);
    }

}