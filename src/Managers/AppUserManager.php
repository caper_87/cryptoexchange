<?php
namespace App\Managers;

use App\Entity\Exchanger;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AppUserManager
{
    private $manager;
    private $em;

    public function __construct(UserManagerInterface $manager, ObjectManager $em)
    {
        $this->manager = $manager;
        $this->em = $em;
    }

    /**
     * @param Form $form
     * @param bool $admin
     * @return mixed
     */
    public function createUserFromForm(Form $form, $admin = false)
    {
        $user = $form->getData();
        $this->isUsernameFree($user->getUsername());
        $user->setEnabled(true);
        $user->addRole($admin ? User::ROLE_ADMIN : User::ROLE_OPERATOR);
        $user->setFakeEmail();
        $user->setHash($user->getUsername());
        $this->setPermissions($user, $form, $admin);
        $this->setExchangers($user, $form, $admin);
        $this->updateUser($user);
        return $this->findUser($user->getId());
    }

    /**
     * @param Form $form
     * @param bool $admin
     * @return mixed
     */
    public function updateUserFromForm(Form $form, $admin = false)
    {
        $user = $form->getData();
        $this->isUsernameFree($user->getUsername(), $user);
        $this->updateExchangers($user, $form, $admin);
        if ($admin) $this->updatePermissions($user, $form);
        $this->updateUser($user);
        $this->reloadUser($user);
        return $this->findUser($user->getId());
    }

    /**
     * @param User $user
     * @param Form $form
     */
    public function setExchangers(User $user, Form $form, $admin = false)
    {
        $exchangersIds = json_decode($form->get('exchangers')->getData(), true);
        if (!is_array($exchangersIds)) {
            throw new BadRequestHttpException("Invalid exchangers json!", null, Response::HTTP_BAD_REQUEST);
        }
        $exchangers = $this->em->getRepository(Exchanger::class)->findBy(['id' => $exchangersIds]);
        if ($exchangers) {
            foreach ($exchangers as $exchanger) {
                $admin ? $exchanger->setAdmin($user) : $exchanger->setOperator($user);
            }
        }
    }

    /**
     * @param User $user
     * @param Form $form
     * @param bool $admin
     */
    public function updateExchangers(User $user, Form $form, $admin = false)
    {
        $exchangersIds = json_decode($form->get('exchangers')->getData(), true);
        if (!is_array($exchangersIds)) {
            return;
        }
        $exchangers = $this->em->getRepository(Exchanger::class)->findBy(['id' => $exchangersIds]);
        $admin ? $this->resetAdminExchangers($user, $exchangers) :
                 $this->resetOperatorExchangers($user, $exchangers);
    }

    /**
     * @param User $user
     * @param $exchangers
     */
    public function resetAdminExchangers(User $user, $exchangers)
    {
        if ($oldExchangers = $user->getAdminExchangers()) {
            foreach ($oldExchangers as $oldExchanger) {
                $oldExchanger->setAdmin(null);
            }
        }
        foreach ($exchangers as $newExchanger) {
            $newExchanger->setAdmin($user);
        }
    }

    /**
     * @param User $user
     * @param $exchangers
     */
    public function resetOperatorExchangers(User $user, $exchangers)
    {
        foreach ($exchangers as $newExchanger) {
            if ($oldExchanger = $user->getOperatorExchanger()) {
                $oldExchanger->setOperator(null);
            }
            $newExchanger->setOperator($user);
        }
    }

    /**
     * @param User $user
     * @param Form $form
     * @param bool $admin
     */
    public function setPermissions(User $user, Form $form, $admin = false)
    {
        if ($admin) {
            $this->setDefaultAdminPermissions($user);
        } else {
            $this->setDefaultOperatorPermissions($user);
            return;
        }

        $permissions = json_decode($form->get('permissions')->getData(), true);
        if (!is_array($permissions)) {
            throw new BadRequestHttpException("Invalid permissions json!", null, Response::HTTP_BAD_REQUEST);
        }
        foreach ($permissions as $permission) {
            $this->isPermissionValid($permission);
            $user->addRole($permission);
        }
    }

    /**
     * @param User $user
     * @param Form $form
     */
    public function updatePermissions(User $user, Form $form)
    {
        $permissions = json_decode($form->get('permissions')->getData(), true);
        if (!is_array($permissions)) {
            return;
        }
        $this->resetPermissions($user);
        foreach ($permissions as $permission) {
            $this->isPermissionValid($permission);
            $user->addRole($permission);
        }
    }

    public function  setDefaultAdminPermissions(User $user)
    {
        $permissions = User::DEFAULT_ADMIN_PERMISSIONS;
        if (!$permissions) return;
        foreach ($permissions as $permission) {
            $this->isPermissionValid($permission);
            $user->addRole($permission);
        }
    }


    public function setDefaultOperatorPermissions(User $user)
    {
        $permissions = User::DEFAULT_OPERATOR_PERMISSIONS;
        if (!$permissions) return;
        foreach ($permissions as $permission) {
            $this->isPermissionValid($permission);
            $user->addRole($permission);
        }
    }

    /**
     * @param User $user
     */
    public function resetPermissions(User $user)
    {
        foreach ($user->getRoles() as $role) {
            if (in_array($role, User::MAIN_ROLES)) continue;
            $user->removeRole($role);
        }
    }

    /**
     * @param string $permission
     * @return bool
     */
    public function isPermissionValid(string $permission) :bool
    {
        if (in_array($permission,User::ALLOWED_PERMISSIONS)) {
            return true;
        }
        throw new BadRequestHttpException('Attempt to set invalid permission!');
    }

    /**
     * @param $username
     * @param null $currentUser
     * @return bool
     */
    public function isUsernameFree($username, $currentUser = null)
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['username' => $username]);
        if ($user && $user != $currentUser) {
            throw new BadRequestHttpException("User with username: ".$username." already exist!", null, Response::HTTP_BAD_REQUEST);
        }
        return true;
    }

    /**
     * @param User $user
     */
    public function updateUser(User $user)
    {
        return $this->manager->updateUser($user);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function reloadUser(User $user)
    {
        return $this->manager->reloadUser($user);
    }

    /**
     * @param User $user
     */
    public function delete(User $user)
    {
        $this->manager->deleteUser($user);
    }

    /**
     * @param User $user
     */
    public function disableUser(User $user)
    {
        //$user->setHash('block');
        $user->setEnabled(!$user->isEnabled());
        $this->updateUser($user);
    }

    public function findUser($id)
    {
        return $this->manager->findUserBy(['id' => $id]);
    }
}