<?php
namespace App\Managers;

use App\Entity\Currency;
use App\Entity\Purse;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Form;

class CurrencyManager extends BaseManager
{
    public function __construct(ObjectManager $em)
    {
        parent::__construct($em);
    }

    public function createFromForm(Form $form)
    {
        /** @var Currency $currency */
        $currency = $form->getData();
        $currency->setEnabled(true);
        $this->save($currency);
        return $currency;
    }

    public function updateFromForm(Form $form)
    {
        /** @var Currency $currency */
        $currency = $form->getData();
        if ($enabled = $form->get('enabled')->getData()) {
            $enabled = $enabled == 'true' ? true : false;
            $currency->setEnabled($enabled);
        }
        $this->save($currency);
        return $currency;
    }

    /**
     * @param Currency $currency
     * @return bool
     */
    public function checkIsCurrencyAvailable(Currency $currency)
    {
        if ($purse = $this->getActivePurse($currency)) {
            return $currency->getEnabled() && $purse->getEnabled();
        }
        return false;
    }

    /**
     * @param Currency $currency
     * @return null|object
     */
    public function getActivePurse(Currency $currency)
    {
        return $this->getRepository(new Purse())->findOneBy([
            'currency' => $currency->getId(),
            'enabled' => true
        ]);
    }
}