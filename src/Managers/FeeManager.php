<?php

namespace App\Managers;

use App\Entity\Currency;
use App\Entity\Fee;
use App\Entity\Transaction;
use App\Form\FeeFormType;
use App\Form\FormHandler;
use App\FormRequest\CalculateAmountRequest;
use App\FormRequest\CalculateCourseRequest;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Yaml\Yaml;

class FeeManager extends BaseManager
{
    /**
     * FeeManager constructor.
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param $fees
     * @param FormHandler $handler
     * @return array|bool
     */
    public function update($fees, FormHandler $handler)
    {
        $repo = $this->getRepository(new Fee());
        foreach ($fees as $feeData) {
            $this->jsonValidate($feeData, 'fee');
            $feeEntity = $repo->find($feeData['id']);

            $feeEntity = $handler->handle(
                FeeFormType::class,
                $feeEntity,
                $feeData,
                ['allow_extra_fields' => true],
                false,
                false
            );

            $this->save($feeEntity);
        }
    }

    /**
     * @param $fees
     * @return array
     */
    public function economicInterestValidate($fees)
    {
        $res = [];
        $fees = array_chunk($fees, 2);
        foreach ($fees as $pair) {
            $res[$pair[1]['currencyIn']] = ($pair[1]['percent'] < $pair[0]['percent']) ? false : true;
        }
        if (in_array(false, $res)) {
            return $res;
        }
    }

    /**
     * @param array $incomeCourses
     * @return float|int|mixed
     */
    public function calculateCourses(array $incomeCourses)
    {
        $res = [];
        foreach ($incomeCourses as $incomeCourse) {
            $this->jsonValidate($incomeCourse, 'course');
            $res[] = [
                'currency' => $incomeCourse['currency'],
                'code' => $incomeCourse['code'],
                'type' => $incomeCourse['type'],
                'course' => $this->calculateCourse($incomeCourse),
            ];
        }
        return $res;
    }

    /**
     * @param array $incomeCourse
     * @return float|int|mixed
     */
    public function calculateCourse(array $incomeCourse)
    {
        $course = (float) $incomeCourse['course'];
        $currency = $this->getCurrency($incomeCourse['currency']);
        $buy = (boolean) $incomeCourse['type'] == Transaction::TYPE_BUY;

        $feeObj = $buy ?
            $this->getFeeForCurrency('USD', $currency->getCode()) :
            $this->getFeeForCurrency($currency->getCode(), 'USD');

        $feePercent = $feeObj->getPercent() / 100;
        $feeDirection = $feeObj->getDirection();

        $appFee = $course * $feePercent;

        if ($feeDirection == Fee::INCREASE) {
            return $course + $appFee;
        }
        return $course - $appFee;
    }

    /**
     * @param CalculateAmountRequest $request
     * @return array
     */
    public function calculateAmount(CalculateAmountRequest $request) //USD
    {
        $crypto = (boolean) $request->getAmountCurrency();
        return $crypto ?
            $this->calculateAmountCrypto($request) :
            $this->calculateAmountUSD($request);

    }

    /**
     * @param CalculateAmountRequest $request
     * @return array
     */
    public function calculateAmountUSD(CalculateAmountRequest $request)
    {
        $amount = (float) $request->getAmount();
        if ($amount <= $this->getStaticFee()) {
            throw new BadRequestHttpException('Введенная сумма меньше минимальной комиссии!');
        }
        $course = 1 / (float) $request->getCourse();
        $currency = $this->getCurrency($request->getCurrency());
        $feeObj = $this->getFeeObj($request->getType(), $currency->getCode());

        $feePercent = $feeObj->getPercent() / 100;
        $feeDirection = $feeObj->getDirection();

        $ourPercentSum = $amount * $feePercent;

        if ($ourPercentSum < $this->getStaticFee()) {
            $finalFee = $this->getStaticFee();
            $finalAmount = $course * ($amount - $finalFee);
            return [
                'amount' => $finalAmount,
                'course' => $this->returnCourse($request->getType(), $finalAmount / $amount)
            ];
        }

        if ($feeDirection == Fee::INCREASE) {
            $finalAmount = $course * ($amount - $ourPercentSum);
            return [
                'amount' => $finalAmount,
                'course' => $this->returnCourse($request->getType(), $finalAmount / $amount)
            ];
        }

        $finalAmount = $course * ($amount + $ourPercentSum);
        return [
            'amount' => $finalAmount,
            'course' => $this->returnCourse($request->getType(), $finalAmount / $amount)
        ];
    }

    /**
     * @param CalculateAmountRequest $request
     * @return array
     */
    public function calculateAmountCrypto(CalculateAmountRequest $request)
    {
        $amount = (float) $request->getAmount();
        $course = (float) $request->getCourse();
        $currency = $this->getCurrency($request->getCurrency());
        $feeObj = $this->getFeeObj($request->getType(), $currency->getCode());

        $feePercent = $feeObj->getPercent() / 100;
        $feeDirection = $feeObj->getDirection();

        $ourPercentSum = ($amount * $course) * $feePercent;

        if ($ourPercentSum < $this->getStaticFee()) {
            $finalFee = $this->getStaticFee();
            $finalAmount = $course * $amount + $finalFee;
            return [
                'amount' => round($finalAmount),
                'course' => $this->returnCourse($request->getType(), $finalAmount / $amount)
            ];
        }

        if ($feeDirection == Fee::INCREASE) {
            $finalAmount = $course * $amount + $ourPercentSum;
            return [
                'amount' => round($finalAmount),
                'course' => $this->returnCourse($request->getType(), $finalAmount / $amount)
            ];
        }

        $finalAmount = $course * $amount - $ourPercentSum;
        return [
            'amount' => round($finalAmount),
            'course' => $this->returnCourse($request->getType(), $finalAmount / $amount)
        ];
    }

    /**
     * @param $type
     * @param $course
     * @return float|int
     */
    public function returnCourse($type, $course)
    {
        return $type == Transaction::TYPE_BUY ? $course : 1 / $course;
    }

    /**
     * @param CalculateAmountRequest $request
     * @return float|int|mixed
     */
    public function calculateAmount2(CalculateAmountRequest $request)
    {
        $amount = (float) $request->getAmount();
        $course = (float) $request->getCourse();
        $currency = $this->getCurrency($request->getCurrency());

        if ($request->getType() == Transaction::TYPE_BUY) {
            $feeObj = $this->getFeeForCurrency('USD', $currency->getCode());
        } else {
            $feeObj = $this->getFeeForCurrency($currency->getCode(), 'USD');
        }
        $feePercent = $feeObj->getPercent() / 100;
        $feeDirection = $feeObj->getDirection();

        $sumWithOurPercent = ($course * $amount) * $feePercent;

        if ($sumWithOurPercent < $this->getStaticFee()) {
            $finalFee = $this->getStaticFee();
            return $course * $amount + $finalFee;
        }

        $finalFee = $course * $feePercent;
        if ($feeDirection == Fee::INCREASE) {
            return $course * $amount + $finalFee;
        }
        return $course * $amount - $finalFee;
    }

    /**
     * @param $type
     * @param $currencyCode
     * @return null|object
     */
    public function getFeeObj($type, $currencyCode)
    {
        if ($type == Transaction::TYPE_BUY) {
            return $this->getFeeForCurrency('USD', $currencyCode);
        }
        return $this->getFeeForCurrency($currencyCode, 'USD');
    }

    /**
     * @param $currencyId
     * @return null|object
     */
    public function getCurrency($currencyId)
    {
        return $this->getRepository(new Currency())->find($currencyId);
    }

    /**
     * @param $currencyIn
     * @param $currencyOut
     * @return null|object
     */
    public function getFeeForCurrency($currencyIn, $currencyOut)
    {
        return $this->getRepository(new Fee())->findOneBy([
            'currencyIn' => $currencyIn,
            'currencyOut' => $currencyOut,
        ]);
    }

    /**
     * @return mixed
     */
    public function getStaticFee()
    {
        $val = Yaml::parseFile(__DIR__ . '/../../public/custom_params.yaml');
        return (float) $val['staticFee'];
    }

    /**
     * @param $fee
     * @return mixed
     */
    public function setStaticFee($fee)
    {
        $array = ['staticFee' => (float) $fee];
        $yaml = Yaml::dump($array);

        file_put_contents(__DIR__ . '/../../public/custom_params.yaml', $yaml);
        return $array;
    }
}