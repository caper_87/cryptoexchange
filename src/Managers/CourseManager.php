<?php

namespace App\Managers;


use App\Entity\Currency;
use App\Entity\Transaction;
use App\Utils\CourseProvider;
use Doctrine\Common\Persistence\ObjectManager;

class CourseManager extends BaseManager
{
    private $provider;
    private $feeManager;

    /**
     * @var array ['BTC' => 1, ...]
     */
    private $activeCurrencies;

    public function __construct(ObjectManager $em, CourseProvider $provider, FeeManager $feeManager)
    {
        parent::__construct($em);
        $this->provider = $provider;
        $this->feeManager = $feeManager;
        $this->activeCurrencies = $this->getActiveCurrencies();
    }

    /**
     * Get array of currencies code and id
     * @return mixed
     */
    public function getActiveCurrencies()
    {
        return $this->getRepository(new Currency())->findDataForBuildCourses();
    }

    /**
     * @return array
     */
    public function getCourses()
    {
        $data = $this->provider->getCourses();
        $rawCourses = $this->buildCourses($data);
        return $rawCourses; // $this->feeManager->calculateCourses($rawCourses);
    }

    /**
     * @param array $data
     * @return array
     */
    public function buildCourses(array $data)
    {
        $courses = [];
        foreach ($data as $item) {
            if ($item['symbol'] == 'DASH') $item['symbol'] = 'DSH';
            $courses[] = $this->buildDirectCourse($item);
            $courses[] = $this->buildReverseCourse($item);
        }
        return $courses;
    }

    /**
     * @param array $item
     * @return array
     */
    public function  buildDirectCourse(array $item)
    {
        return [
            'course' => $item['quotes']['USD']['price'],
            'currency' => $this->activeCurrencies[$item['symbol']],
            'code' => $item['symbol'],
            'type' => Transaction::TYPE_BUY,
        ];
    }

    /**
     * @param array $item
     * @return array
     */
    public function  buildReverseCourse(array $item)
    {
        return [
            'course' => 1 / $item['quotes']['USD']['price'],
            'currency' => $this->activeCurrencies[$item['symbol']],
            'code' => $item['symbol'],
            'type' => Transaction::TYPE_SELL,
        ];
    }


}