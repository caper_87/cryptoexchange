<?php

namespace App\Managers;

use App\Entity\Currency;
use App\Entity\Exchanger;
use App\Entity\Fee;
use App\Entity\Purse;
use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Yaml\Yaml;

class TransactionManager extends BaseManager
{
    private $feeManager;

    public function __construct(ObjectManager $em, FeeManager $feeManager)
    {
        parent::__construct($em);
        $this->feeManager = $feeManager;
    }

    /**
     * @param Form $form
     * @param User $operator
     * @return Transaction
     */
    public function createTransaction(Form $form, User $operator)
    {
        /** @var Transaction $transaction */
        $transaction = $form->getData();
        /** @var Exchanger $exchanger */
        $exchanger = $this->exchangerValidate($operator->getOperatorExchanger());

        $this->isAdminAndOperatorAvailable($exchanger);

        $transaction->setExchanger($exchanger);
        $transaction->setAdmin($exchanger->getAdmin());
        $transaction->setOperator($operator);
        $transaction->setStatus(Transaction::STATUS_NEW);

        $this->setCurrentCourseProvider($transaction);
        $this->setFee($transaction);
        $this->setPurseOwn($transaction);

        $this->save($transaction);
        return $transaction;
    }

    /**
     * @param Transaction $transaction
     * @param int $status
     * @return Transaction
     */
    public function updateStatus(Transaction $transaction, int $status)
    {
        if (!in_array($status, Transaction::AVAILABLE_STATUSES)) {
            throw new BadRequestHttpException('Попытка присвоить неверный статус');
        }
        $transaction->setStatus($status);
        if ($status == Transaction::STATUS_DONE) {
            $transaction->setFinishedAt(new \DateTime());
        }
        $this->save($transaction);
        return $transaction;
    }

    public function exchangerValidate($exchanger = null)
    {
        if (!$exchanger || !$exchanger->getEnabled() || !$exchanger->getAdmin() || !$exchanger->getOperator()) {
            throw new BadRequestHttpException('Данный обменник недоступен');
        }
        return $exchanger;
    }

    public function isAdminAndOperatorAvailable(Exchanger $exchanger)
    {
        $operator = $exchanger->getOperator();
        $admin = $exchanger->getAdmin();
        if (!$admin || !$operator || !$admin->getEnabled() || !$operator->getEnabled()) {
            throw new BadRequestHttpException('Админ или оператор данного обменника недоступен');
        }
    }

    /**
     * @param $currencyId
     * @return bool
     */
    public function checkIsCurrencyAvailable($currencyId)
    {
        /** @var Currency $currency */
        $currency = $this->getRepository(new Currency())->find($currencyId);
        if ($purse = $this->getActivePurse($currency)) {
            return $currency->getEnabled() && $purse->getEnabled();
        }
        return false;
    }

    /**
     * @param Currency $currency
     * @return null|object
     */
    public function getActivePurse(Currency $currency)
    {
        return $this->getRepository(new Purse())->findOneBy([
            'currency' => $currency->getId(),
            'enabled' => true
        ]);
    }

    /**
     * @param Transaction $transaction
     */
    public function setCurrentCourseProvider(Transaction $transaction)
    {
        $transaction->setCourseProvider('Bitfinex');
    }

    /**
     * @param Transaction $transaction
     * @return Transaction
     */
    public function  setFee(Transaction $transaction)
    {
        /** @var Currency $currency */
        $currency = $transaction->getCurrency();
        $course = $transaction->getCourse();
        $amount = $transaction->getIncome();

        if ($transaction->getType() == Transaction::TYPE_BUY) {
            $feeObj = $this->getFeeForCurrency('USD', $currency->getCode());
            $feePercent = $feeObj->getPercent() / 100;
            $feeDirection = $feeObj->getDirection();
            $ourPercentSum = $amount * $feePercent;
            if ($ourPercentSum < $this->getStaticFee()) {
                $transaction->setFee($this->getStaticFee());
                return $transaction;
            }
            if ($feeDirection == Fee::INCREASE) {
                $transaction->setFee($ourPercentSum);
                return $transaction;
            }
            $transaction->setFee(-1 * $ourPercentSum);
            return $transaction;
        } else {
            $feeObj = $this->getFeeForCurrency($currency->getCode(), 'USD');
            $feePercent = $feeObj->getPercent() / 100;
            $feeDirection = $feeObj->getDirection();
            $ourPercentSum = ($amount * $course) * $feePercent;
            if ($ourPercentSum < $this->getStaticFee()) {
                $transaction->setFee($this->getStaticFee());
                return $transaction;
            }
            if ($feeDirection == Fee::INCREASE) {
                $transaction->setFee($ourPercentSum);
                return $transaction;
            }
            $transaction->setFee(-1 * $ourPercentSum);
            return $transaction;
        }
    }

    public function getFeeForCurrency($currencyIn, $currencyOut)
    {
        return $this->getRepository(new Fee())->findOneBy([
            'currencyIn' => $currencyIn,
            'currencyOut' => $currencyOut,
        ]);
    }

    public function getStaticFee()
    {
        $val = Yaml::parseFile(__DIR__ . '/../../public/custom_params.yaml');
        return $val['staticFee'];
    }

    /**
     * @param Transaction $transaction
     */
    public function setPurseOwn(Transaction $transaction)
    {
        $purse = $this->getPurseByCode($transaction->getCurrency());
        $transaction->setPurseOwn($purse);
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getPurseByCode($code)
    {
        return $this->getRepository(new Purse())->findOneByCode($code);
    }
}