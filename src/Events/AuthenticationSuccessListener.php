<?php

namespace App\Events;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof User) {
            return;
        }

        $data = [
            'status' => true,
            'data' => [
                'token' => $data['token'],
                'user' => $user->getModelForApi()
            ]
        ];

        if (!$this->checkIsOperatorExchangerEnabled($user)) {
            $data = [
                'status' => false,
                'code' => Response::HTTP_FORBIDDEN,
                'errors' => [
                    'message' => 'Данный обменний заблокирован!'
                ]
            ];
        }

        if (!$this->checkOperatorIP($user)) {
            $data = [
                'status' => false,
                'code' => Response::HTTP_FORBIDDEN,
                'errors' => [
                    'message' => 'Ваш IP адресс не соответствует адресу обменника!'
                ]
            ];
        }

        $event->setData($data);
    }

    public function checkOperatorIP(User $user)
    {
        if (!$user->hasRole(User::ROLE_OPERATOR)) return true;
        if ($exchanger = $user->getOperatorExchanger()) {
            return  $exchanger->getIp() == $_SERVER['REMOTE_ADDR'];
        }
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function checkIsOperatorExchangerEnabled(User $user)
    {
        if (!$user->hasRole(User::ROLE_OPERATOR)) return true;
        if (!$exchanger = $user->getOperatorExchanger()) return true;
        return $exchanger->getEnabled();
    }
}