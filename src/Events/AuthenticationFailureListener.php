<?php

namespace App\Events;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationFailureListener
{
    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $data = [
            'status' => false,
            'code'  => Response::HTTP_FORBIDDEN,
            'errors' => ['message' => 'Неверный логин или пароль!']
        ];

        $response = new JsonResponse($data, Response::HTTP_FORBIDDEN);
        $event->setResponse($response);
    }
}