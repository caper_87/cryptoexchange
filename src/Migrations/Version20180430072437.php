<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180430072437 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_fee (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, value_percent DOUBLE PRECISION DEFAULT NULL, value_default DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_purse (id INT AUTO_INCREMENT NOT NULL, currency VARCHAR(255) NOT NULL, token VARCHAR(1000) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', phone VARCHAR(255) DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, hash VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_88BDF3E992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_88BDF3E9A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_88BDF3E9C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_exchanger (id INT AUTO_INCREMENT NOT NULL, admin_id INT NOT NULL, operator_id INT DEFAULT NULL, country VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, street VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, ip VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_ED6C5280642B8210 (admin_id), UNIQUE INDEX UNIQ_ED6C5280584598A3 (operator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_transaction (id INT AUTO_INCREMENT NOT NULL, exchanger_id INT NOT NULL, operator_id INT NOT NULL, admin_id INT NOT NULL, income DOUBLE PRECISION NOT NULL, outcome DOUBLE PRECISION NOT NULL, course DOUBLE PRECISION NOT NULL, course_provider VARCHAR(255) DEFAULT NULL, currency VARCHAR(255) NOT NULL, purse VARCHAR(1000) NOT NULL, purse_own VARCHAR(1000) DEFAULT NULL, status INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, finished_at DATETIME DEFAULT NULL, fee DOUBLE PRECISION DEFAULT NULL, INDEX IDX_2BD2236D8973D679 (exchanger_id), INDEX IDX_2BD2236D584598A3 (operator_id), INDEX IDX_2BD2236D642B8210 (admin_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_exchanger ADD CONSTRAINT FK_ED6C5280642B8210 FOREIGN KEY (admin_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE app_exchanger ADD CONSTRAINT FK_ED6C5280584598A3 FOREIGN KEY (operator_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE app_transaction ADD CONSTRAINT FK_2BD2236D8973D679 FOREIGN KEY (exchanger_id) REFERENCES app_exchanger (id)');
        $this->addSql('ALTER TABLE app_transaction ADD CONSTRAINT FK_2BD2236D584598A3 FOREIGN KEY (operator_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE app_transaction ADD CONSTRAINT FK_2BD2236D642B8210 FOREIGN KEY (admin_id) REFERENCES app_user (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_exchanger DROP FOREIGN KEY FK_ED6C5280642B8210');
        $this->addSql('ALTER TABLE app_exchanger DROP FOREIGN KEY FK_ED6C5280584598A3');
        $this->addSql('ALTER TABLE app_transaction DROP FOREIGN KEY FK_2BD2236D584598A3');
        $this->addSql('ALTER TABLE app_transaction DROP FOREIGN KEY FK_2BD2236D642B8210');
        $this->addSql('ALTER TABLE app_transaction DROP FOREIGN KEY FK_2BD2236D8973D679');
        $this->addSql('DROP TABLE app_fee');
        $this->addSql('DROP TABLE app_purse');
        $this->addSql('DROP TABLE app_user');
        $this->addSql('DROP TABLE app_exchanger');
        $this->addSql('DROP TABLE app_transaction');
    }
}
