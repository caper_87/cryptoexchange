<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180516101825 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_transaction ADD purse_own_id INT DEFAULT NULL, DROP purse_own');
        $this->addSql('ALTER TABLE app_transaction ADD CONSTRAINT FK_2BD2236D5647395C FOREIGN KEY (purse_own_id) REFERENCES app_purse (id)');
        $this->addSql('CREATE INDEX IDX_2BD2236D5647395C ON app_transaction (purse_own_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_transaction DROP FOREIGN KEY FK_2BD2236D5647395C');
        $this->addSql('DROP INDEX IDX_2BD2236D5647395C ON app_transaction');
        $this->addSql('ALTER TABLE app_transaction ADD purse_own VARCHAR(1000) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP purse_own_id');
    }
}
