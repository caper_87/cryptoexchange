<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180517111329 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_transaction ADD currency_id INT DEFAULT NULL, DROP currency');
        $this->addSql('ALTER TABLE app_transaction ADD CONSTRAINT FK_2BD2236D38248176 FOREIGN KEY (currency_id) REFERENCES app_currency (id)');
        $this->addSql('CREATE INDEX IDX_2BD2236D38248176 ON app_transaction (currency_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_transaction DROP FOREIGN KEY FK_2BD2236D38248176');
        $this->addSql('DROP INDEX IDX_2BD2236D38248176 ON app_transaction');
        $this->addSql('ALTER TABLE app_transaction ADD currency VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP currency_id');
    }
}
