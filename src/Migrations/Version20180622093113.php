<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180622093113 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_message (id INT AUTO_INCREMENT NOT NULL, dialog_id INT DEFAULT NULL, user_id INT DEFAULT NULL, body VARCHAR(1000) NOT NULL, status VARCHAR(255) NOT NULL, date_create DATETIME NOT NULL, date_end DATETIME NOT NULL, INDEX IDX_5BE0B0325E46C4E2 (dialog_id), INDEX IDX_5BE0B032A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_dialog_user (id INT AUTO_INCREMENT NOT NULL, dialog_id INT DEFAULT NULL, user_id INT DEFAULT NULL, last_message_id INT DEFAULT NULL, status VARCHAR(255) NOT NULL, date_create DATETIME NOT NULL, date_end DATETIME NOT NULL, INDEX IDX_4D5A2FE25E46C4E2 (dialog_id), INDEX IDX_4D5A2FE2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_dialog (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_message ADD CONSTRAINT FK_5BE0B0325E46C4E2 FOREIGN KEY (dialog_id) REFERENCES app_dialog (id)');
        $this->addSql('ALTER TABLE app_message ADD CONSTRAINT FK_5BE0B032A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE app_dialog_user ADD CONSTRAINT FK_4D5A2FE25E46C4E2 FOREIGN KEY (dialog_id) REFERENCES app_dialog (id)');
        $this->addSql('ALTER TABLE app_dialog_user ADD CONSTRAINT FK_4D5A2FE2A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_message DROP FOREIGN KEY FK_5BE0B0325E46C4E2');
        $this->addSql('ALTER TABLE app_dialog_user DROP FOREIGN KEY FK_4D5A2FE25E46C4E2');
        $this->addSql('DROP TABLE app_message');
        $this->addSql('DROP TABLE app_dialog_user');
        $this->addSql('DROP TABLE app_dialog');
    }
}
