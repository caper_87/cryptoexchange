<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180523121618 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_fee ADD currency_in VARCHAR(255) NOT NULL, ADD currency_out VARCHAR(255) NOT NULL, ADD percent DOUBLE PRECISION DEFAULT NULL, ADD direction INT NOT NULL, DROP title, DROP slug, DROP value_percent, DROP value_default');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_fee ADD title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD slug VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD value_default DOUBLE PRECISION DEFAULT NULL, DROP currency_in, DROP currency_out, DROP direction, CHANGE percent value_percent DOUBLE PRECISION DEFAULT NULL');
    }
}
