<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180516093209 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_currency (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_purse ADD currency_id INT DEFAULT NULL, DROP currency');
        $this->addSql('ALTER TABLE app_purse ADD CONSTRAINT FK_DB7DC96D38248176 FOREIGN KEY (currency_id) REFERENCES app_currency (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DB7DC96D38248176 ON app_purse (currency_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_purse DROP FOREIGN KEY FK_DB7DC96D38248176');
        $this->addSql('DROP TABLE app_currency');
        $this->addSql('DROP INDEX UNIQ_DB7DC96D38248176 ON app_purse');
        $this->addSql('ALTER TABLE app_purse ADD currency VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP currency_id');
    }
}
