<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAllUsers($sort, $direction)
    {
        return $this->createQueryBuilder('u')
            ->orderBy("u.{$sort}", $direction)
            ->getQuery()
            ->getResult();
    }

    public function findOneOrFail($id)
    {
        $user = $this->find($id);
        if (!$user) {
            throw new NotFoundHttpException("Record not found!", null, 404);
        }
        return $user;
    }

    public function findOneByOrFail($field, $value)
    {
        $user = $this->createQueryBuilder('u')
            ->andWhere("u.{$field} = :val")
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
        if (!$user) {
            throw new NotFoundHttpException("Record not found!");
        }
        return $user;
    }

    public function findByRole($role, $sort, $direction)
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"'.$role.'"%')
            ->orderBy("u.{$sort}", $direction)
            ->getQuery()
            ->getResult();
    }

    public function findOneByRole($role)
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"'.$role.'"%')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

//    /**
//     * @return User[] Returns an array of User objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
