<?php

namespace App\Repository;

use App\Entity\Exchanger;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Exchanger|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exchanger|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exchanger[]    findAll()
 * @method Exchanger[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExchangerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Exchanger::class);
    }

    public function findAllExchangers($sort, $direction)
    {
        return $this->createQueryBuilder('e')
            ->orderBy("e.{$sort}", $direction)
            ->getQuery()
            ->getResult();
    }

    /**
     * Return Available exchangers by user role
     * @param bool $admin
     * @param $sort
     * @param $direction
     * @return mixed
     */
    public function  findAllAvailable(bool $admin, $sort, $direction)
    {
        $role = $admin ? 'admin' : 'operator';
        return $this->createQueryBuilder('e')
            ->where("e.{$role} IS NULL")
            ->andWhere("e.enabled = 1")
            ->orderBy("e.{$sort}", $direction)
            ->getQuery()
            ->getResult();
    }

}
