<?php

namespace App\Repository;

use App\Entity\Purse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Purse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Purse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Purse[]    findAll()
 * @method Purse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Purse::class);
    }


    public function findOneByCode($currency)
    {
        return $this->findOneBy(['currency' => $currency, 'enabled' => true]);
    }

//    /**
//     * @return Purse[] Returns an array of Purse objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Purse
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
