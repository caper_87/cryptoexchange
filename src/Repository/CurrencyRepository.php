<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\Purse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Currency|null find($id, $lockMode = null, $lockVersion = null)
 * @method Currency|null findOneBy(array $criteria, array $orderBy = null)
 * @method Currency[]    findAll()
 * @method Currency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrencyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Currency::class);
    }

    public function findAllWithPurses()
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.purse', 'p', 'c.id=p.currency_id')
            ->getQuery()
            ->getResult();
    }

    public function findAllWithoutPurses()
    {
        $currencies = $this->createQueryBuilder('c')
            ->leftJoin('c.purse', 'p', 'c.id=p.currency_id')
            ->where('p.currency IS NULL')
            ->orWhere('p.enabled = 0')
            ->getQuery()
            ->getResult();

        // filter currency with enabled purses
        if ($currencies) {
           foreach ($currencies as $key => $currency) {
               /** @var Purse $purse */
               $purses = $currency->getPurse();
               if (!$purses) continue;

               $enabledPurseExist = $purses->exists(function($key, $element) {
                   return true === $element->getEnabled();
               });

               if ($enabledPurseExist) unset($currencies[$key]);
           }
        }
        return array_values($currencies);
    }

    public function findOneWithActivePurse($id)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.purse', 'p', 'c.id=p.currency_id')
            ->where('c.id =:id')
            ->andWhere('p.enabled = 1')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    public function findDataForBuildCourses()
    {
        $sql = "SELECT c.code , c.id FROM app_currency c ;";
        $sql = $this->getEntityManager()->getConnection()->prepare($sql);
        $sql->execute();
        $data = $sql->fetchAll(\PDO::FETCH_ASSOC);

        $result = [];
        foreach ($data as $item) {
            $result[$item['code']] = (int) $item['id'];
        }
        return $result;
    }

//    /**
//     * @return Currency[] Returns an array of Currency objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Currency
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
