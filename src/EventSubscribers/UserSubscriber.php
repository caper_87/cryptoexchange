<?php

namespace App\EventSubscribers;

use App\Entity\User;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class UserSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            Events::preRemove,
        );
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $user = $args->getObject();
        $em = $args->getObjectManager();

        if ($user instanceof User) {
            $user->hasRole(User::ROLE_ADMIN) ?
                $this->adminDetaching($user, $em) :
                $this->operatorDetaching($user, $em);
        }

    }

    public function adminDetaching(User $user, $em)
    {
        $this->detachExchangers($user, $em);
    }

    public function operatorDetaching(User $user, $em)
    {
        $exchanger = $user->getOperatorExchanger();
        if ($exchanger) {
            $exchanger->setOperator(null);
            $em->persist($exchanger);
            $em->flush();
        }
    }

    public function detachExchangers(User $user, $em)
    {
        $exchangers = $user->getAdminExchangers();
        if ($exchangers) {
            foreach ($exchangers as $exchanger) {
                $exchanger->setAdmin(null);
                $em->persist($exchanger);
            }
            $em->flush();
        }
    }

}