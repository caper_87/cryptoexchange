<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use App\Entity\Exchanger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCurrencyData extends Fixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $currency = new Currency();
        $currency->setTitle('Bitcoin');
        $currency->setCode('BTC');
        $currency->setEnabled(true);
        $this->addReference('BTC', $currency);
        $manager->persist($currency);
        $manager->flush();

        $currency = new Currency();
        $currency->setTitle('DASH');
        $currency->setCode('DSH');
        $currency->setEnabled(true);
        $this->addReference('DASH', $currency);
        $manager->persist($currency);
        $manager->flush();

        $currency = new Currency();
        $currency->setTitle('USDT');
        $currency->setCode('USDT');
        $currency->setEnabled(true);
        $this->addReference('USDT', $currency);
        $manager->persist($currency);
        $manager->flush();

        $currency = new Currency();
        $currency->setTitle('Ethereum');
        $currency->setCode('ETH');
        $currency->setEnabled(true);
        $this->addReference('ETH', $currency);
        $manager->persist($currency);
        $manager->flush();

        $currency = new Currency();
        $currency->setTitle('ZCASH');
        $currency->setCode('ZEC');
        $currency->setEnabled(true);
        $this->addReference('ZCASH', $currency);
        $manager->persist($currency);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadUserData::class,
        );
    }
}
