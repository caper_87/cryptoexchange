<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use FOS\UserBundle\Model\UserManagerInterface;

class LoadUserData extends Fixture
{
    public $userManager;
    public $faker;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
        $this->faker = Factory::create();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $testPassword = '123321q';

        $user = new User();
        $user->setUsername('super.admin');
        $user->setFirstName($this->faker->firstName);
        $user->setLastName($this->faker->lastName);
        $user->setPlainPassword($testPassword);
        $user->setEmail("super.admin@example.com");
        $user->setPhone($this->faker->phoneNumber);
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $user->setEnabled(true);
        $user->setHash($user->getUsername());
        $this->addReference('super_admin', $user);
        $this->userManager->updateUser($user);

        $user = new User();
        $user->setUsername('admin');
        $user->setFirstName($this->faker->firstName);
        $user->setLastName($this->faker->lastName);
        $user->setPlainPassword($testPassword);
        $user->setEmail("admin@example.com");
        $user->setPhone($this->faker->phoneNumber);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEnabled(true);
        $user->setHash($user->getUsername());
        $this->addReference('admin', $user);
        $this->userManager->updateUser($user);

        $user = new User();
        $user->setUsername('operator1');
        $user->setFirstName($this->faker->firstName);
        $user->setLastName($this->faker->lastName);
        $user->setPlainPassword($testPassword);
        $user->setEmail("operator1@example.com");
        $user->setPhone($this->faker->phoneNumber);
        $user->setRoles(['ROLE_OPERATOR', 'ROLE_CREATE_TRANSACTION']);
        $user->setEnabled(true);
        $user->setHash($user->getUsername());
        $this->addReference('operator1', $user);
        $this->userManager->updateUser($user);

        $user = new User();
        $user->setUsername('operator2');
        $user->setFirstName($this->faker->firstName);
        $user->setLastName($this->faker->lastName);
        $user->setPlainPassword($testPassword);
        $user->setEmail("operator2@example.com");
        $user->setPhone($this->faker->phoneNumber);
        $user->setRoles(['ROLE_OPERATOR', 'ROLE_CREATE_TRANSACTION']);
        $user->setEnabled(true);
        $user->setHash($user->getUsername());
        $this->addReference('operator2', $user);
        $this->userManager->updateUser($user);

        $user = new User();
        $user->setUsername('operator3');
        $user->setFirstName($this->faker->firstName);
        $user->setLastName($this->faker->lastName);
        $user->setPlainPassword($testPassword);
        $user->setEmail("operator3@example.com");
        $user->setPhone($this->faker->phoneNumber);
        $user->setRoles(['ROLE_OPERATOR', 'ROLE_CREATE_TRANSACTION']);
        $user->setEnabled(true);
        $user->setHash($user->getUsername());
        $this->addReference('operator3', $user);
        $this->userManager->updateUser($user);
    }
}
