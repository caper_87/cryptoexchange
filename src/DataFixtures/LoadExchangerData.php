<?php

namespace App\DataFixtures;

use App\Entity\Exchanger;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadExchangerData extends Fixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $exchanger = new Exchanger();
        $exchanger->setCountry('USA');
        $exchanger->setCity('NY');
        $exchanger->setStreet('Broadway 1/2');
        $exchanger->setPhone('123123123');
        $exchanger->setIp('123.12.12.12');
        $exchanger->setEnabled(true);
        $exchanger->setAdmin($this->getReference('admin'));
        $exchanger->setOperator($this->getReference('operator1'));
        $this->addReference('exchanger1', $exchanger);
        $manager->persist($exchanger);
        $manager->flush();

        $exchanger = new Exchanger();
        $exchanger->setCountry('Canada');
        $exchanger->setCity('Toronto');
        $exchanger->setStreet('Broadway 1/2');
        $exchanger->setPhone('123123123');
        $exchanger->setIp('123.12.12.12');
        $exchanger->setEnabled(true);
        $exchanger->setAdmin($this->getReference('admin'));
        $exchanger->setOperator($this->getReference('operator2'));
        $this->addReference('exchanger2', $exchanger);
        $manager->persist($exchanger);
        $manager->flush();

        $exchanger = new Exchanger();
        $exchanger->setCountry('Spain');
        $exchanger->setCity('Valencia');
        $exchanger->setStreet('Broadway 1/2');
        $exchanger->setPhone('123123123');
        $exchanger->setIp('123.12.12.12');
        $exchanger->setEnabled(true);
        $exchanger->setAdmin($this->getReference('admin'));
        $exchanger->setOperator($this->getReference('operator3'));
        $this->addReference('exchanger3', $exchanger);
        $manager->persist($exchanger);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadUserData::class,
        );
    }
}
