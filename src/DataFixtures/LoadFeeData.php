<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use App\Entity\Exchanger;
use App\Entity\Fee;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFeeData extends Fixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /**
         * BTC
         */
        $fee = new Fee();
        $fee->setCurrencyIn('USD');
        $fee->setCurrencyOut('BTC');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_USD_BTC', $fee);
        $manager->persist($fee);
        $manager->flush();

        $fee = new Fee();
        $fee->setCurrencyIn('BTC');
        $fee->setCurrencyOut('USD');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_BTC_USD', $fee);
        $manager->persist($fee);
        $manager->flush();

        /**
         * DASH
         */
        $fee = new Fee();
        $fee->setCurrencyIn('USD');
        $fee->setCurrencyOut('DSH');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_USD_DASH', $fee);
        $manager->persist($fee);
        $manager->flush();

        $fee = new Fee();
        $fee->setCurrencyIn('DSH');
        $fee->setCurrencyOut('USD');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_DASH_USD', $fee);
        $manager->persist($fee);
        $manager->flush();

        /**
         * USDT
         */
        $fee = new Fee();
        $fee->setCurrencyIn('USD');
        $fee->setCurrencyOut('USDT');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_USD_USDT', $fee);
        $manager->persist($fee);
        $manager->flush();

        $fee = new Fee();
        $fee->setCurrencyIn('USDT');
        $fee->setCurrencyOut('USD');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_USDT_USD', $fee);
        $manager->persist($fee);
        $manager->flush();

        /**
         * ETH
         */
        $fee = new Fee();
        $fee->setCurrencyIn('USD');
        $fee->setCurrencyOut('ETH');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_USD_ETH', $fee);
        $manager->persist($fee);
        $manager->flush();

        $fee = new Fee();
        $fee->setCurrencyIn('ETH');
        $fee->setCurrencyOut('USD');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_ETH_USD', $fee);
        $manager->persist($fee);
        $manager->flush();

        /**
         * ZCASH
         */
        $fee = new Fee();
        $fee->setCurrencyIn('USD');
        $fee->setCurrencyOut('ZEC');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_USD_ZCASH', $fee);
        $manager->persist($fee);
        $manager->flush();

        $fee = new Fee();
        $fee->setCurrencyIn('ZEC');
        $fee->setCurrencyOut('USD');
        $fee->setDirection(Fee::INCREASE);
        $fee->setPercent(1.1);
        $this->addReference('fee_ZCASH_USD', $fee);
        $manager->persist($fee);
        $manager->flush();

    }

}
