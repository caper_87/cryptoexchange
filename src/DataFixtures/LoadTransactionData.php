<?php

namespace App\DataFixtures;

use App\Entity\Transaction;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTransactionData extends Fixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $transaction = new Transaction();
        $transaction->setAdmin($this->getReference('admin'));
        $transaction->setOperator($this->getReference('operator1'));
        $transaction->setStatus(Transaction::STATUS_NEW);
        $transaction->setType(Transaction::TYPE_BUY);
        $transaction->setIncome(1.22);
        $transaction->setOutcome(10.50);
        $transaction->setFee(1.2);
        $transaction->setCurrency($this->getReference('DASH'));
        $transaction->setCourse(5.6);
        $transaction->setCourseProvider('Provider 1');
        $transaction->setExchanger($this->getReference('exchanger1'));
        $transaction->setPurse('token');
        $transaction->setPurseOwn($this->getReference('purse_DASH'));
        $this->addReference('transaction1', $transaction);
        $manager->persist($transaction);
        $manager->flush();

        $transaction = new Transaction();
        $transaction->setAdmin($this->getReference('admin'));
        $transaction->setOperator($this->getReference('operator2'));
        $transaction->setStatus(Transaction::STATUS_NEW);
        $transaction->setType(Transaction::TYPE_BUY);
        $transaction->setIncome(1.22);
        $transaction->setOutcome(10.50);
        $transaction->setFee(1.2);
        $transaction->setCurrency($this->getReference('ETH'));
        $transaction->setCourse(5.6);
        $transaction->setCourseProvider('Provider 1');
        $transaction->setExchanger($this->getReference('exchanger2'));
        $transaction->setPurse('token');
        $transaction->setPurseOwn($this->getReference('purse_ETH'));
        $this->addReference('transaction2', $transaction);
        $manager->persist($transaction);
        $manager->flush();

        $transaction = new Transaction();
        $transaction->setAdmin($this->getReference('admin'));
        $transaction->setOperator($this->getReference('operator3'));
        $transaction->setStatus(Transaction::STATUS_DECLINE);
        $transaction->setType(Transaction::TYPE_SELL);
        $transaction->setIncome(1.22);
        $transaction->setOutcome(10.50);
        $transaction->setFee(1.2);
        $transaction->setCurrency($this->getReference('BTC'));
        $transaction->setCourse(5.6);
        $transaction->setCourseProvider('Provider 1');
        $transaction->setExchanger($this->getReference('exchanger3'));
        $transaction->setPurse('token');
        $transaction->setPurseOwn($this->getReference('purse_BTC'));
        $this->addReference('transaction3', $transaction);
        $manager->persist($transaction);
        $manager->flush();

        $transaction = new Transaction();
        $transaction->setAdmin($this->getReference('admin'));
        $transaction->setOperator($this->getReference('operator2'));
        $transaction->setStatus(Transaction::STATUS_NEW);
        $transaction->setType(Transaction::TYPE_BUY);
        $transaction->setIncome(1.22);
        $transaction->setOutcome(10.50);
        $transaction->setFee(1.2);
        $transaction->setCurrency($this->getReference('USDT'));
        $transaction->setCourse(5.6);
        $transaction->setCourseProvider('Provider 1');
        $transaction->setExchanger($this->getReference('exchanger1'));
        $transaction->setPurse('token');
        $transaction->setPurseOwn($this->getReference('purse_USDT'));
        $this->addReference('transaction4', $transaction);
        $manager->persist($transaction);
        $manager->flush();

        $transaction = new Transaction();
        $transaction->setAdmin($this->getReference('admin'));
        $transaction->setOperator($this->getReference('operator1'));
        $transaction->setStatus(Transaction::STATUS_NEW);
        $transaction->setType(Transaction::TYPE_BUY);
        $transaction->setIncome(1.22);
        $transaction->setOutcome(10.50);
        $transaction->setFee(1.2);
        $transaction->setCurrency($this->getReference('ZCASH'));
        $transaction->setCourse(5.6);
        $transaction->setCourseProvider('Provider 1');
        $transaction->setExchanger($this->getReference('exchanger1'));
        $transaction->setPurse('token');
        $transaction->setPurseOwn($this->getReference('purse_ZCASH'));
        $this->addReference('transaction5', $transaction);
        $manager->persist($transaction);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadUserData::class,
            LoadExchangerData::class,
            LoadPurseData::class
        );
    }
}
