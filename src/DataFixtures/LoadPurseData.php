<?php

namespace App\DataFixtures;

use App\Entity\Exchanger;
use App\Entity\Purse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPurseData extends Fixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $purse = new Purse();
        $purse->setToken(md5(rand(1,100)));
        $purse->setCurrency($this->getReference('BTC'));
        $purse->setEnabled(true);
        $manager->persist($purse);
        $manager->flush();
        $this->addReference('purse_BTC', $purse);

        $purse = new Purse();
        $purse->setToken(md5(rand(1,100)));
        $purse->setCurrency($this->getReference('DASH'));
        $purse->setEnabled(true);
        $manager->persist($purse);
        $manager->flush();
        $this->addReference('purse_DASH', $purse);

        $purse = new Purse();
        $purse->setToken(md5(rand(1,100)));
        $purse->setCurrency($this->getReference('USDT'));
        $purse->setEnabled(true);
        $manager->persist($purse);
        $manager->flush();
        $this->addReference('purse_USDT', $purse);

        $purse = new Purse();
        $purse->setToken(md5(rand(1,100)));
        $purse->setCurrency($this->getReference('ETH'));
        $purse->setEnabled(true);
        $manager->persist($purse);
        $manager->flush();
        $this->addReference('purse_ETH', $purse);

        $purse = new Purse();
        $purse->setToken(md5(rand(1,100)));
        $purse->setCurrency($this->getReference('ZCASH'));
        $purse->setEnabled(true);
        $manager->persist($purse);
        $manager->flush();
        $this->addReference('purse_ZCASH', $purse);
    }

    public function getDependencies()
    {
        return array(
            LoadCurrencyData::class,
        );
    }
}
