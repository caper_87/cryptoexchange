<?php

namespace App\FormRequest;

use Symfony\Component\Validator\Constraints as Assert;

class CalculateCourseRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $course;

    /**
     * @Assert\NotBlank()
     */
    private $currency;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/(1|2)/")
     */
    private $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param mixed $course
     */
    public function setCourse($course): void
    {
        $this->course = $course;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }


}