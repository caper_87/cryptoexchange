<?php
namespace App\FormRequest;

use Symfony\Component\Validator\Constraints as Assert;

class CalculateAmountRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $amount;

    /**
     * 1 => current currency
     * 0 => USD
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/(0|1)/")
     */
    private $amountCurrency;

    /**
     * @Assert\NotBlank()
     */
    private $course;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/(1|2)/")
     */
    private $type;

    /**
     * @Assert\NotBlank()
     */
    private $currency;

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmountCurrency()
    {
        return $this->amountCurrency;
    }

    /**
     * @param mixed $amountCurrency
     */
    public function setAmountCurrency($amountCurrency): void
    {
        $this->amountCurrency = $amountCurrency;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param mixed $course
     */
    public function setCourse($course): void
    {
        $this->course = $course;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

}