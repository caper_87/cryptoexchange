<?php

namespace App\Exception;

use Symfony\Component\Form\Form;

class InvalidFormException extends \RuntimeException
{
    const DEFAULT_ERROR_MESSAGE = "The submitted data was invalid.";

    /**
     * @var Form
     */
    protected $form;

    /**
     * @param null $form
     * @param string $message
     */
    public function __construct($form = null, $message = self::DEFAULT_ERROR_MESSAGE)
    {
        $message = $this->getErrors($form)[0] ?? $message;
        parent::__construct($message, 403);
        $this->form = $form;
    }

    /**
     * @param Form|array $form
     * @return array
     */
    public function getErrors($form)
    {
        $errors = [];
        if ($form instanceof Form) {
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
            foreach ($form->all() as $child) {
                if ($err = $this->getErrors($child)) {
                    $errors[] = $err[0];
                }
            }
        }

        return $errors;
    }
}