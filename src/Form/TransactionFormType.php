<?php

namespace App\Form;


use App\Entity\Currency;
use App\Entity\Exchanger;
use App\Entity\Transaction;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('income', NumberType::class, [
                'invalid_message' => 'Недопустимое значение для входящей суммы'
            ])
            ->add('outcome', NumberType::class, [
                'invalid_message' => 'Недопустимое значение для исходящей суммы'
            ])
            ->add('type', IntegerType::class, [
                'invalid_message' => 'Недопустимое значение для типа сделки, доступно 1 || 2'
            ])
            ->add('course', TextType::class, [
                'invalid_message' => 'Недопустимое значение для курса'
            ])
            ->add('currency', EntityType::class, [
                'class' => Currency::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.enabled = true');
                },
                'invalid_message' => 'Обмен данной валюты временно недоступен'
            ])
            ->add('purse', TextType::class, [
                'required' => false,
                'invalid_message' => 'Недопустимое значение для кошелька'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }

}