<?php

namespace App\Form;

use App\Entity\Fee;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('currencyIn', TextType::class, [
                'invalid_message' => 'Недопустимое значение для валюты'
            ])
            ->add('currencyOut', TextType::class, [
                'invalid_message' => 'Недопустимое значение для валюты'
            ])
            ->add('direction', IntegerType::class, [
                'invalid_message' => 'Недопустимое значение для увеличения/уменьшения ставки'
            ])
            ->add('percent', TextType::class, [
                'required' => false,
                'invalid_message' => 'Недопустимое значение для процентная ставка'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fee::class,
        ]);
    }
}