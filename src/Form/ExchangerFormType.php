<?php

namespace App\Form;


use App\Entity\Exchanger;
use App\Entity\Transaction;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExchangerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country', TextType::class, [
                'invalid_message' => 'Недопустимое значение для страны'
            ])
            ->add('city', TextType::class, [
                'invalid_message' => 'Недопустимое значение для города'
            ])
            ->add('street', TextType::class, [
                'invalid_message' => 'Недопустимое значение для адреса'
            ])
            ->add('phone', TextType::class, [
                'invalid_message' => 'Недопустимое значение для телефона'
            ])
            ->add('ip', TextType::class, [
                'invalid_message' => 'Недопустимое значение для IP адреса'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Exchanger::class,
        ]);
    }
}