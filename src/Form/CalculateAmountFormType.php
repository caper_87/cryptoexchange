<?php

namespace App\Form;

use App\FormRequest\CalculateAmountRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalculateAmountFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', TextType::class, [
                'invalid_message' => 'Недопустимое значение для суммы'
            ])
            ->add('amountCurrency', IntegerType::class, [
                'invalid_message' => 'Недопустимое значение для входящей валюты'
            ])
            ->add('course', TextType::class, [
                'invalid_message' => 'Недопустимое значение для курса'
            ])
            ->add('type', IntegerType::class, [
                'invalid_message' => 'Недопустимое значение для типа сделки'
            ])
            ->add('currency',TextType::class, [
                'invalid_message' => 'Недопустимое значение для валюты'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CalculateAmountRequest::class,
        ]);
    }
}