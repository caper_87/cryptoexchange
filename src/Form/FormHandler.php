<?php

namespace App\Form;

use App\Exception\InvalidFormException;
use Symfony\Component\Form\FormFactoryInterface;

class FormHandler
{

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * FormHandler constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param $type
     * @param $object
     * @param array $parameters
     * @param array $options
     * @param bool $clearMissing
     * @param bool $returnForm
     * @return \Symfony\Component\Form\FormInterface | object
     */
    public function handle($type, $object, array $parameters, array $options = [], $clearMissing = true, $returnForm = true)
    {
        $options = array_replace_recursive([
            'csrf_protection' => false,
        ], $options);

        $form = $this->formFactory->create($type, $object, $options);

        $form->submit($parameters, $clearMissing);

        if (!$form->isValid()) {
            throw new InvalidFormException($form);
        }

        if ($returnForm) {
            return $form;
        } else {
            return $form->getData();
        }
    }
}