<?php

namespace App\Form;

use App\FormRequest\CalculateCourseRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalculateCourseFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('course', TextType::class, [
                'invalid_message' => 'Недопустимое значение для курса'
            ])
            ->add('currency',TextType::class, [
                'invalid_message' => 'Недопустимое значение для валюты'
            ])
            ->add('type',IntegerType::class, [
                'invalid_message' => 'Недопустимое значение для типа сделки'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CalculateCourseRequest::class,
        ]);
    }
}