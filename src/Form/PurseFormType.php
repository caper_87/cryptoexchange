<?php

namespace App\Form;

use App\Entity\Currency;
use App\Entity\Purse;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PurseFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('token', TextType::class, [
                'invalid_message' => 'Недопустимое значение для токена'
            ])
            ->add('currency', EntityType::class, [
                'class' => Currency::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.purse', 'p', 'c.id=p.currency_id')
                        ->where('p.currency IS NULL')
                        ->orWhere('p.enabled = 0');
                },
                'invalid_message' => 'Недопустимое значение для валюты'
            ])
            ->add('password', TextType::class, [
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Purse::class,
        ]);
    }
}