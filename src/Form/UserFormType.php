<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'invalid_message' => 'Недопустимое значение для логина'
            ])
            ->add('plainPassword', PasswordType::class, [
                'invalid_message' => 'Недопустимое значение для пароля'
            ])
            ->add('firstName', TextType::class, [
                'invalid_message' => 'Недопустимое значение для имени'
            ])
            ->add('lastName', TextType::class, [
                'invalid_message' => 'Недопустимое значение для фамилии'
            ])
            ->add('phone', TextType::class, [
                'invalid_message' => 'Недопустимое значение для телефона'
            ])
            ->add('exchangers', TextType::class, ['mapped' => false,])
            ->add('permissions', TextType::class, ['mapped' => false,]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}