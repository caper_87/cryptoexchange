<?php

namespace App\Utils;


use GuzzleHttp\Client;
use Symfony\Component\PropertyAccess\Exception\AccessException;

class CourseProvider
{
    const SOURCE_URL = 'https://api.coinmarketcap.com/v2/ticker/';

    const CURRENCIES_IDS = [
        1,    // BTC
        1027, // ETH
        825,  // USDT
        131,  // DSH
        1437, // ZEC
    ];

    /**
     * @var Client
     */
    private $httpClient;

    public function initClient()
    {
        $this->httpClient = new Client();
    }

    /**
     * @param null $url
     * @return array
     */
    public function getCourses($url = null)
    {
        $url = $url ?? self::SOURCE_URL;
        $this->initClient();
        $response = $this->httpClient->get($url);
        $rawData = $this->readResponseData($response, 'data');
        return $this->filterUselessData($rawData);
    }

    /**
     * @param $data
     * @return array
     */
    public function filterUselessData($data)
    {
        $allowed = self::CURRENCIES_IDS;
        $filtered = array_filter(
            $data,
            function ($key) use ($allowed) {
                return in_array($key, $allowed);
            },
            ARRAY_FILTER_USE_KEY
        );
        return $filtered;
    }

    /**
     * @param $response
     * @param $property
     * @return mixed
     */
    protected function readResponseData($response, $property)
    {
        $content = json_decode($response->getBody()->getContents(), true);
        try {
            return $content[$property];
        } catch (AccessException $e) {
            $values = is_array($content) ? $content : get_object_vars($content);
            throw new AccessException(sprintf(
                'Error reading property "%s" from available keys (%s)',
                $property,
                implode(', ', array_keys($values))
            ), 0, $e);
        }
    }
}