<?php

namespace App\Utils;

use JsonSchema\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class JsonValidator
{
    private $validator;
    private $schema;

    public function __construct()
    {
        $this->validator = new Validator();
    }

    public function getSchema(string $schemaName)
    {
        $json = file_get_contents(__DIR__.'/../../public/schemas/'.$schemaName.'.json');
        if (!$json) {
            throw new BadRequestHttpException("Schema file not found!");
        }
        $this->schema = (object) json_decode($json, true);
    }

    public function validate($modelData, string $schemaName)
    {
        $this->getSchema($schemaName);
        $this->validator->validate($modelData, $this->schema);
        if ($this->validator->isValid()) {
            return true;
        }
        return false;
    }
}