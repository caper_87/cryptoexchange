<?php

namespace App\Traits;

use Knp\Component\Pager\Paginator;

trait PaginationTrait
{
    public $defaultSort = 'id';
    public $defaultDirection = 'asc';

    public function paginate($data, $page, $maxItems)
    {
        /** @var Paginator $pagination */
        $pagination = $this->get('knp_paginator')->paginate($data, $page, $maxItems);
        $meta = $pagination->getPaginationData();
        return [
            'currentPage'    => (integer) $meta['current'],
            'pageCount'      => (integer) $meta['pageCount'],
            'totalItemCount' => (integer) $meta['totalCount'],
            'items'          => $pagination->getItems(),
        ];
    }
}