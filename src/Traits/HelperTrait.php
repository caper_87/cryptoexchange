<?php

namespace App\Traits;

use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

trait HelperTrait
{
    /**
     * @var TranslatorInterface $translator
     */
    public $translator;

    /**
     * HelperTrait constructor.
     * @required
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param \Exception $exception
     * @return View
     */
    public function riseError(\Exception $exception)
    {
        $code = $exception->getCode() != 0 ? $exception->getCode() : Response::HTTP_BAD_REQUEST;
        $message = $this->translateErrorMessage($exception, $code);
        return new View([
            'status' => false,
            'code'  => $code,
            'errors' => [
                'message' => $message
            ]
        ], $code);
    }

    /**
     * @param array $data
     * @param bool $status
     * @param int $code
     * @return View
     */
    public function returnSuccess(array $data, $status = true, $code = 200)
    {
        return new View([
            'status' => $status,
            'data' => $data
        ], $code);
    }

    /**
     * @param \Exception $exception
     * @param $code
     * @return string
     */
    public function translateErrorMessage(\Exception $exception, $code)
    {
        if (preg_match("/[а-яё]/iu", $exception->getMessage())) {
            return $exception->getMessage();
        }

        switch ($code) {
            case 400:
                return $this->translator->trans('security.bed');
            case 404:
                return $this->translator->trans('security.not_found');
            case 500:
                return $this->translator->trans('security.internal');
            default:
                return $exception->getMessage();
        }
    }
}