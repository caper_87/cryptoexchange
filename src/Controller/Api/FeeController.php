<?php

namespace App\Controller\Api;

use App\Entity\Fee;
use App\Form\CalculateAmountFormType;
use App\Form\CalculateCourseFormType;
use App\Form\FeeFormType;
use App\Form\FormHandler;
use App\FormRequest\CalculateAmountRequest;
use App\FormRequest\CalculateCourseRequest;
use App\Managers\CourseManager;
use App\Managers\FeeManager;
use App\Repository\FeeRepository;
use App\Traits\HelperTrait;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use FOS\RestBundle\Controller\Annotations as Rest;

class FeeController extends FOSRestController
{
    use HelperTrait;

    /**
     * @var FeeRepository
     */
    public $repository;

    public function __construct(FeeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Find all Fees
     *
     * @SWG\Tag(name="Fees")
     * @Route("/api/fees", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find all Fees ",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Fee::class, groups={"full"}))
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @return View
     */
    public function index()
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_FEE', null, $this->get('translator')->trans('security.access_denied'));
            $fees = $this->repository->findAll();
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($fees);
    }

    /**
     * Find Fee by ID
     *
     * @SWG\Tag(name="Fees")
     * @Route("/api/fee/{id}", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find Fee by ID",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Fee::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param $id
     * @return View
     */
    public function show($id)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_FEE', null, $this->get('translator')->trans('security.access_denied'));
            if (!$fee = $this->repository->find($id)) {
                throw new NotFoundHttpException(null, null, Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$fee]);
    }

    /**
     * Get static fee
     *
     * @SWG\Tag(name="Fees")
     * @Route("/api/fee/static/", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get static fee",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Fee::class, groups={"full"}))
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param FeeManager $manager
     * @return View
     */
    public function getStaticFee(FeeManager $manager)
    {
        try {
            $fee = $manager->getStaticFee();
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess(['staticFee' => $fee]);
    }

    /**
    * Set static Fee
    *
    * @SWG\Post(
    *    consumes={"application/x-www-form-urlencoded"},
    *    produces={"application/json"},
    *    path="/api/fee/static",
    *    tags={"Fees"},
    *    @SWG\Parameter(
    *        in="formData",
    *        name="staticFee",
    *        required=true,
    *        type="number"
    *    ),
    *    @SWG\Response(
    *        response=200,
    *        description="Set static Fee",
    *        @SWG\Schema(
    *            type="object",
    *            @SWG\Items(ref=@Model(type=Fee::class, groups={"full"}))
    *        )
    *    )
    * )
    * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
    * @Route("/api/fee/static", methods={"POST"})
    * @param Request $request
    * @param FeeManager $manager
    * @return View
    */
    public function setStaticFee(Request $request, FeeManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_FEE', null, $this->get('translator')->trans('security.access_denied'));
            $fee = $manager->setStaticFee($request->get('staticFee'));
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$fee]);
    }

    /**
     * Get Reserve Courses
     *
     * @SWG\Tag(name="Course")
     * @Route("/api/courses", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get Reserve Courses",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Fee::class, groups={"full"}))
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param CourseManager $manager
     * @return View
     */
    public function getReserveCourse(CourseManager $manager)
    {
        try {
            $courses = $manager->getCourses();
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($courses);
    }

    /**
     * Calculate course
     *
     * @SWG\Post(
     *
     *    produces={"application/json"},
     *    path="/api/fee/course/calculate",
     *    tags={"Fees"},
     *    @SWG\Parameter(
     *        in="body",
     *        name="data",
     *        required=true,
     *        @SWG\Schema(type="string")
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Calculate course",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Fee::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/fee/course/calculate", methods={"POST"})
     * @param Request $request
     * @param FeeManager $manager
     * @return View
     */
    public function calculateCourse(Request $request, FeeManager $manager)
    {
        try {
            $courses = $manager->calculateCourses($request->request->all());
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($courses);
    }

    /**
     * Calculate Amount
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/fee/calculate",
     *    tags={"Fees"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="amount",
     *        required=true,
     *        type="number"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="amountCurrency",
     *        required=true,
     *        type="integer"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="course",
     *        required=true,
     *        type="number"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="type",
     *        required=true,
     *        type="integer"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="currency",
     *        required=true,
     *        type="string"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Calculate Amount",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Fee::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/fee/calculate", methods={"POST"})
     * @param Request $request
     * @param FeeManager $manager
     * @return View
     */
    public function calculateAmount(Request $request, FeeManager $manager)
    {
        try {
            $obj = $this->get(FormHandler::class)
                ->handle(CalculateAmountFormType::class, new CalculateAmountRequest(), $request->request->all(), [], false, false);
            $amount = $manager->calculateAmount($obj);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($amount);
    }

    /**
     * Update Fee
     *
     * @SWG\Put(
     *
     *    produces={"application/json"},
     *    path="/api/fee/update",
     *    tags={"Fees"},
     *    @SWG\Parameter(
     *        in="body",
     *        name="data",
     *        required=true,
     *        @SWG\Schema(type="string")
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Update Fee",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Fee::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/fee/update", methods={"PUT"})
     * @param Request $request
     * @param FeeManager $manager
     * @return View
     */
    public function update(Request $request, FeeManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_FEE', null, $this->get('translator')->trans('security.access_denied'));
            $fees = $request->request->all();
            if ($errors = $manager->economicInterestValidate($fees)) {
                return $this->returnSuccess(['economic_validate_errors' => $errors]);
            }
            $manager->update($fees, $this->get(FormHandler::class));
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess(['message' => $this->get('translator')->trans('fee.update_success')]);
    }
}
