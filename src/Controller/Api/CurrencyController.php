<?php

namespace App\Controller\Api;

use App\Entity\Currency;
use App\Entity\Purse;
use App\Entity\User;
use App\Form\CurrencyFormType;
use App\Form\ExchangerFormType;
use App\Form\FormHandler;
use App\Form\PurseFormType;
use App\Managers\CurrencyManager;
use App\Managers\PurseManager;
use App\Repository\CurrencyRepository;
use App\Repository\PurseRepository;
use App\Traits\HelperTrait;
use App\Traits\PaginationTrait;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Exchanger;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;

class CurrencyController extends FOSRestController
{
    use PaginationTrait, HelperTrait;

    /**
     * @var PurseRepository
     */
    public $repository;

    public function __construct(CurrencyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Find all Currencies
     *
     * @SWG\Tag(name="Currencies")
     * @Route("/api/currencies", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find all Currencies ",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Currency::class, groups={"full"}))
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @return View
     */
    public function index()
    {
        try {
//            $this->denyAccessUnlessGranted('ROLE_MANAGE_CURRENCIES', null, $this->get('translator')->trans('security.access_denied'));
            $currencies = $this->repository->findAllWithPurses();
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($currencies);
    }

    /**
     * Find all Currencies without attached purses
     *
     * @SWG\Tag(name="Currencies")
     * @Route("/api/currencies/available", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find all Currencies without attached purses ",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Currency::class, groups={"full"}))
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @return View
     */
    public function indexAvailable()
    {
        try {
//            $this->denyAccessUnlessGranted('ROLE_MANAGE_CURRENCIES', null, $this->get('translator')->trans('security.access_denied'));
            $currencies = $this->repository->findAllWithoutPurses();
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($currencies);
    }

    /**
     * Find Currency by ID
     *
     * @SWG\Tag(name="Currencies")
     * @Route("/api/currency/{id}", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find Currency by ID",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Currency::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param $id
     * @return View
     */
    public function show($id)
    {
        try {
//            $this->denyAccessUnlessGranted('ROLE_MANAGE_CURRENCIES', null, $this->get('translator')->trans('security.access_denied'));
            if (!$currency = $this->repository->find($id)) {
                throw new NotFoundHttpException(null, null, Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$currency]);
    }

    /**
     * Create Currency
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/currency",
     *    tags={"Currencies"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="title",
     *        required=true,
     *        type="string",
     *        minimum=2,
     *        maximum=1000
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="code",
     *        required=true,
     *        type="string",
     *        minimum=2,
     *        maximum=64
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Create Currency",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Currency::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/currency", methods={"POST"})
     * @param Request $request
     * @param CurrencyManager $manager
     * @return View
     */
    public function create(Request $request, CurrencyManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_CURRENCIES', null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                ->handle(CurrencyFormType::class, new Currency(), $request->request->all());
            $currency = $manager->createFromForm($form);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$currency]);
    }

    /**
     * Update Currency
     *
     * @SWG\Put(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/currency/{id}/update",
     *    tags={"Currencies"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="title",
     *        required=false,
     *        type="string",
     *        minimum=2,
     *        maximum=1000
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="code",
     *        required=false,
     *        type="string",
     *        minimum=2,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="enabled",
     *        required=false,
     *        type="boolean"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Update Currency",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Currency::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/currency/{id}/update", methods={"PUT"})
     * @param Currency $currency
     * @param Request $request
     * @param CurrencyManager $manager
     * @return View
     */
    public function update(Currency $currency, Request $request, CurrencyManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_CURRENCIES', null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                ->handle(CurrencyFormType::class, $currency, $request->request->all(), [], false);
            $currency = $manager->updateFromForm($form);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$currency]);
    }

    /**
     * Check is currency available for make transactions
     *
     * @SWG\Tag(name="Currencies")
     * @Route("/api/currency/{id}/available", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Check is currency available for make transactions",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Currency::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param Currency $currency
     * @param CurrencyManager $manager
     * @return View
     */
    public function checkIsCurrencyAvailable(Currency $currency, CurrencyManager $manager)
    {
        try {
            if (!$manager->checkIsCurrencyAvailable($currency)) {
                throw new BadRequestHttpException($this->get('translator')->trans('transaction.currency_disabled'), null, Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess(['available' => true]);
    }

    /**
     * Delete Currency
     *
     * @SWG\Tag(name="Currencies")
     * @Route("/api/currency/{id}/delete", methods={"DELETE"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *    in="path",
     *    name="id",
     *    required=true,
     *    type="integer"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Delete Currency",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Currency::class, groups={"full"}))
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param Currency $currency
     * @param CurrencyManager $manager
     * @return View
     */
    public function delete(Currency $currency, CurrencyManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_CURRENCIES', null, $this->get('translator')->trans('security.access_denied'));
            $manager->delete($currency);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess(['message' => $this->get('translator')->trans('currency.disable')]);
    }
}
