<?php

namespace App\Controller\Api;

use App\Form\FormHandler;
use App\Form\UserFormType;
use App\Managers\AppUserManager;
use App\Repository\UserRepository;
use App\Traits\HelperTrait;
use App\Traits\PaginationTrait;
use FOS\RestBundle\Controller\FOSRestController;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;

class UserController extends FOSRestController
{
    use PaginationTrait, HelperTrait;

    /**
     * @var UserRepository
     */
    public $userRepository;

    public function __construct(UserRepository $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * Find all users
     *
     * @SWG\Tag(name="Users")
     * @Route("/api/users/{page}/{maxItems}", defaults={"page"=1, "maxItems"=10}, methods={"GET"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *      in="query",
     *      name="sort",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Parameter(
     *      in="query",
     *      name="direction",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Find all users ",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param  $page
     * @param $maxItems
     * @param Request $request
     * @return View
     */
    public function index($page, $maxItems, Request $request)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $users = $this->userRepository->findAllUsers(
                $request->query->get('sort') ?? $this->defaultSort ,
                $request->query->get('direction') ?? $this->defaultDirection
            );
        } catch (\Exception $exception) {
            return $this->riseError($exception);
        }
        return $this->returnSuccess($this->paginate($users, $page, $maxItems));
    }

    /**
     * Find all admins
     *
     * @SWG\Tag(name="Users")
     * @Route("/api/users/admin/{page}/{maxItems}", defaults={"page"=1, "maxItems"=10}, methods={"GET"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *      in="query",
     *      name="sort",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Parameter(
     *      in="query",
     *      name="direction",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Find all admins ",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param  $page
     * @param $maxItems
     * @param Request $request
     * @return View
     */
    public function indexAdmin($page, $maxItems, Request $request)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $users = $this->userRepository->findByRole(
                'ROLE_ADMIN',
                $request->query->get('sort') ?? $this->defaultSort ,
                $request->query->get('direction') ?? $this->defaultDirection
            );
        } catch (\Exception $exception) {
            return $this->riseError($exception);
        }
        return $this->returnSuccess($this->paginate($users, $page, $maxItems));
    }

    /**
     * Find all operators
     *
     * @SWG\Tag(name="Users")
     * @Route("/api/users/operator/{page}/{maxItems}", defaults={"page"=1, "maxItems"=10}, methods={"GET"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *      in="query",
     *      name="sort",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Parameter(
     *      in="query",
     *      name="direction",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Find all operators ",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param  $page
     * @param $maxItems
     * @param Request $request
     * @return View
     */
    public function indexOperator($page, $maxItems, Request $request)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $users = $this->userRepository->findByRole(
                'ROLE_OPERATOR',
                $request->query->get('sort') ?? $this->defaultSort ,
                $request->query->get('direction') ?? $this->defaultDirection
                );
        } catch (\Exception $exception) {
            return $this->riseError($exception);
        }
        return $this->returnSuccess($this->paginate($users, $page, $maxItems));
    }

    /**
     * Find user by ID
     *
     * @SWG\Tag(name="Users")
     * @Route("/api/user/{id}", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find user by ID",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param $id
     * @return View
     */
    public function show($id)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            if (!$user = $this->userRepository->find($id)) {
                throw new NotFoundHttpException(null, null, Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $exception) {
            return $this->riseError($exception);
        }

        return $this->returnSuccess([$user]);
    }

    /**
     * Create admin
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/user/admin",
     *    tags={"Users"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="username",
     *        required=true,
     *        type="string",
     *        minimum=4,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="plainPassword",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="firstName",
     *        required=true,
     *        type="string",
     *        minimum=2,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="lastName",
     *        required=true,
     *        type="string",
     *        minimum=2,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="phone",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="exchangers",
     *        required=true,
     *        type="string"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="permissions",
     *        required=true,
     *        type="string"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Create admin",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/user/admin", methods={"POST"})
     * @param Request $request
     * @param AppUserManager $userManager
     * @return View
     */
    public function createAdmin(Request $request, AppUserManager $userManager)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                         ->handle(UserFormType::class, new User(), $request->request->all());
            $user = $userManager->createUserFromForm($form, true);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }

        return $this->returnSuccess([$user]);
    }

    /**
     * Create operator
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/user/operator",
     *    tags={"Users"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="username",
     *        required=true,
     *        type="string",
     *        minimum=4,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="plainPassword",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="firstName",
     *        required=true,
     *        type="string",
     *        minimum=2,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="lastName",
     *        required=true,
     *        type="string",
     *        minimum=2,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="phone",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="exchangers",
     *        required=true,
     *        type="string"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Create operator",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/user/operator", methods={"POST"})
     * @param Request $request
     * @param AppUserManager $userManager
     * @return View
     */
    public function createOperator(Request $request, AppUserManager $userManager)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                         ->handle(UserFormType::class, new User(), $request->request->all());
            $user = $userManager->createUserFromForm($form);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }

        return $this->returnSuccess([$user]);
    }

    /**
     * Update user
     *
     * @SWG\Put(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/user/{id}/update",
     *    tags={"Users"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="username",
     *        required=false,
     *        type="string",
     *        minimum=4,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="plainPassword",
     *        required=false,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="firstName",
     *        required=false,
     *        type="string",
     *        minimum=4,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="lastName",
     *        required=false,
     *        type="string",
     *        minimum=4,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="phone",
     *        required=false,
     *        type="string",
     *        minimum=4,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="exchangers",
     *        required=false,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="permissions",
     *        required=false,
     *        type="string"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Update user",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/user/{id}/update", methods={"PUT"})
     * @param User $user
     * @param Request $request
     * @param AppUserManager $userManager
     * @return View
     */
    public function update(User $user, Request $request, AppUserManager $userManager)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                         ->handle(UserFormType::class, $user, $request->request->all(), [], false);
            $user = $userManager->updateUserFromForm($form, $user->hasRole(User::ROLE_ADMIN));
        } catch (\Exception $e) {
            return $this->riseError($e);
        }

        return $this->returnSuccess([$user]);
    }

    /**
     * Disable user
     *
     * @SWG\Tag(name="Users")
     * @Route("/api/user/{id}/disable", methods={"DELETE"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *    in="path",
     *    name="id",
     *    required=true,
     *    type="integer"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Disable user",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param User $user
     * @param AppUserManager $userManager
     * @return View
     */
    public function disable(User $user, AppUserManager $userManager)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $userManager->disableUser($user);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess(['message' => $this->get('translator')->trans('exchanger.disable')]);
    }
}
