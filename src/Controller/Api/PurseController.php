<?php

namespace App\Controller\Api;

use App\Entity\Purse;
use App\Entity\User;
use App\Form\ExchangerFormType;
use App\Form\FormHandler;
use App\Form\PurseFormType;
use App\Managers\PurseManager;
use App\Repository\PurseRepository;
use App\Traits\HelperTrait;
use App\Traits\PaginationTrait;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Exchanger;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;

class PurseController extends FOSRestController
{
    use PaginationTrait, HelperTrait;

    /**
     * @var PurseRepository
     */
    public $repository;

    public function __construct(PurseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Find all Purses
     *
     * @SWG\Tag(name="Purses")
     * @Route("/api/purses", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find all Purses ",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Purse::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @return View
     */
    public function index()
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_PURSES', null, $this->get('translator')->trans('security.access_denied'));
            $purses = $this->repository->findAll();
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($purses);
    }

    /**
     * Find Purse by ID
     *
     * @SWG\Tag(name="Purses")
     * @Route("/api/purse/{id}", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find Purse by ID",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Purse::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param $id
     * @return View
     */
    public function show($id)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_PURSES', null, $this->get('translator')->trans('security.access_denied'));
            if (!$purse = $this->repository->find($id)) {
                throw new NotFoundHttpException(null, null, Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$purse]);
    }

    /**
     * Find Purse by Currency
     *
     * @SWG\Tag(name="Purses")
     * @Route("/api/purse/currency/{currencyId}", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find Purse by Currency",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Purse::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param $currencyId
     * @return View
     */
    public function getByCurrency($currencyId)
    {
        try {
            if (!$purse = $this->repository->findOneBy(['currency' => $currencyId, 'enabled' => true])) {
                throw new NotFoundHttpException(null, null, Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$purse]);
    }

    /**
     * Create Purse
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/purse",
     *    tags={"Purses"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="token",
     *        required=true,
     *        type="string",
     *        minimum=2,
     *        maximum=1000
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="currency",
     *        required=true,
     *        type="integer"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="password",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=100
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Create Purse",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Purse::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/purse", methods={"POST"})
     * @param Request $request
     * @param PurseManager $manager
     * @return View
     */
    public function create(Request $request, PurseManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_PURSES', null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                ->handle(PurseFormType::class, new Purse(), $request->request->all());

            if (!$this->get('security.password_encoder')->isPasswordValid($this->getUser(), $request->get('password'))) {
                throw new BadRequestHttpException($this->get('translator')->trans('purse.invalid_pass'), null, Response::HTTP_BAD_REQUEST);
            }

            if ($manager->isCurrencyNotAvailable($request->get('currency'))) {
                throw new BadRequestHttpException($this->get('translator')->trans('purse.currency_used'), null, Response::HTTP_BAD_REQUEST);
            }

            $exchanger = $manager->createFromForm($form);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }

        return $this->returnSuccess([$exchanger]);
    }

    /*
     * Update Purse
     *
     * @SWG\Put(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/purse/{id}/update",
     *    tags={"Purses"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="token",
     *        required=false,
     *        type="string",
     *        minimum=2,
     *        maximum=1000
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Update Purse",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Purse::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/purse/{id}/update", methods={"PUT"})
     * @param Purse $purse
     * @param Request $request
     * @param PurseManager $manager
     * @return View
     */
    public function update(Purse $purse, Request $request, PurseManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_PURSES');
            $form = $this->get(FormHandler::class)
                ->handle(PurseFormType::class, $purse, $request->request->all(), [], false);
            $exchanger = $manager->createFromForm($form);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }

        return $this->returnSuccess([$exchanger]);
    }

    /**
     * Delete Purse
     *
     * @SWG\Tag(name="Purses")
     * @Route("/api/purse/{id}/disable", methods={"DELETE"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *    in="path",
     *    name="id",
     *    required=true,
     *    type="integer"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Delete Purse",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Purse::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param Purse $purse
     * @param PurseManager $manager
     * @return View
     */
    public function disable(Purse $purse, PurseManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_MANAGE_PURSES', null, $this->get('translator')->trans('security.access_denied'));
            if ($transactions = $manager->getOpenTransactions($purse)) {
                $message = $this->get('translator')->trans('purse.disable_error') . implode(',', $transactions);
                throw new BadRequestHttpException($message, null, Response::HTTP_BAD_REQUEST);
            }
            $manager->disable($purse);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess(['message' => $this->get('translator')->trans('purse.disable_success')]);
    }
}
