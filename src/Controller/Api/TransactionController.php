<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Form\FormHandler;
use App\Form\TransactionFormType;
use App\Managers\TransactionManager;
use App\Repository\TransactionRepository;
use App\Traits\HelperTrait;
use App\Traits\PaginationTrait;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Transaction;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TransactionController extends FOSRestController
{
    use PaginationTrait, HelperTrait;

    /**
     * @var TransactionRepository
     */
    public $repository;

    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Find all Transactions
     *
     * @SWG\Tag(name="Transactions")
     * @Route("/api/transactions/{page}/{maxItems}", defaults={"page"=1, "maxItems"=10}, methods={"GET"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *      in="query",
     *      name="sort",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Parameter(
     *      in="query",
     *      name="direction",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Find all Transactions ",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Transaction::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param  $page
     * @param $maxItems
     * @param Request $request
     * @return View
     */
    public function index($page, $maxItems, Request $request)
    {
        try {
            $this->denyAccessUnlessGranted([User::ROLE_SUPER_ADMIN, 'ROLE_WATCHER'], null, $this->get('translator')->trans('security.access_denied'));
            $transactions = $this->repository->findAllTransactions(
                $request->query->get('sort') ?? $this->defaultSort ,
                $request->query->get('direction') ?? $this->defaultDirection
            );
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($this->paginate($transactions, $page, $maxItems));
    }

    /**
     * Find all Transactions for User
     *
     * @SWG\Tag(name="Transactions")
     * @Route("/api/transactions/user/{page}/{maxItems}", defaults={"page"=1, "maxItems"=10}, methods={"GET"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *      in="query",
     *      name="sort",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Parameter(
     *      in="query",
     *      name="direction",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Find all Transactions for User",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Transaction::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"})
     * @param  $page
     * @param $maxItems
     * @param Request $request
     * @return View
     */
    public function indexByUser($page, $maxItems, Request $request)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_VIEW_TRANSACTION', null, $this->get('translator')->trans('security.access_denied'));
            $transactions = $this->repository->findAllByUser(
                $this->getUser(),
                $request->query->get('sort') ?? $this->defaultSort ,
                $request->query->get('direction') ?? $this->defaultDirection
            );
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($this->paginate($transactions, $page, $maxItems));
    }

    /**
     * Find Transaction by ID
     *
     * @SWG\Tag(name="Transactions")
     * @Route("/api/transaction/{id}", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find Transaction by ID",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Transaction::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)

     * @param $id
     * @return View
     */
    public function show($id)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_VIEW_TRANSACTION', null, $this->get('translator')->trans('security.access_denied'));
            if (!$transaction = $this->repository->find($id)) {
                throw new NotFoundHttpException(null, null, Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$transaction]);
    }

    /**
     * Create Transaction
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/transaction",
     *    tags={"Transactions"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="income",
     *        required=true,
     *        type="number"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="outcome",
     *        required=true,
     *        type="number"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="type",
     *        required=true,
     *        type="integer"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="course",
     *        required=true,
     *        type="number"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="currency",
     *        required=true,
     *        type="integer"
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="purse",
     *        required=false,
     *        type="string",
     *        minimum=6,
     *        maximum=1000
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Create Transaction",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Transaction::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/transaction", methods={"POST"})
     * @param Request $request
     * @param TransactionManager $manager
     * @return View
     */
    public function create(Request $request, TransactionManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_CREATE_TRANSACTION', null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                         ->handle(TransactionFormType::class, new Transaction(), $request->request->all());

            if (!$manager->checkIsCurrencyAvailable($request->get('currency'))) {
                throw new BadRequestHttpException($this->get('translator')->trans('transaction.currency_disabled'), null, Response::HTTP_BAD_REQUEST);
            }

            $transaction = $manager->createTransaction($form, $this->getUser());
        } catch (\Exception $e) {
            return $this->riseError($e);
        }

        return $this->returnSuccess([$transaction]);
    }

    /**
     * Update status
     *
     * @SWG\Put(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/transaction/{id}/status",
     *    tags={"Transactions"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="status",
     *        required=true,
     *        type="integer"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Update status",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Transaction::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/transaction/{id}/status", methods={"PUT"})
     * @param Transaction $transaction
     * @param Request $request
     * @param TransactionManager $manager
     * @return View
     */
    public function updateStatus(Transaction $transaction, Request $request, TransactionManager $manager)
    {
        try {
            if ($this->getUser()->hasRole('ROLE_WATCHER') || !$this->allowStatusChange($transaction)) {
                throw new AccessDeniedException($this->get('translator')->trans('security.access_denied'));
            }
            $this->denyAccessUnlessGranted([User::ROLE_SUPER_ADMIN, 'ROLE_UPDATE_TRANSACTION'], null, $this->get('translator')->trans('security.access_denied'));
            $transaction = $manager->updateStatus($transaction, $request->get('status'));
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$transaction]);
    }

    public function allowStatusChange(Transaction $transaction)
    {
        if ($transaction->getStatus() != Transaction::STATUS_NEW) {
            return $this->getUser()->hasRole(User::ROLE_SUPER_ADMIN);
        }
        return true;
    }
}
