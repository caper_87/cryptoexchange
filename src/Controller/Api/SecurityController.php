<?php

namespace App\Controller\Api;

use App\Exception\InvalidFormException;
use App\Form\FormHandler;
use App\Form\LoginFormType;
use App\Form\RegisterFormType;
use App\FormRequest\LoginRequest;
use App\Repository\UserRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;

class SecurityController extends FOSRestController
{
    public $userRepository;

    public function __construct(UserRepository $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * Login user
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/login",
     *    tags={"Security"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="username",
     *        required=true,
     *        type="string",
     *        minimum=4,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="password",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Login user",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"})
     * @Route("/api/login", methods={"POST"})
     * @param Request $request
     * @return View
     */
    public function login(Request $request)
    {
        $this->get(FormHandler::class)
            ->handle(LoginFormType::class, new LoginRequest(), $request->request->all());

        if (!$user = $this->userRepository->findOneByOrFail('username', $request->get('username'))) {
            throw $this->createNotFoundException();
        }

        if (!$isValid = $this->get('security.password_encoder')->isPasswordValid($user, $request->get('password'))) {
            throw new BadRequestHttpException('Invalid credentials', null, Response::HTTP_BAD_REQUEST);
        }

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => time() + 3600 // 1 hour expiration
            ]);

        return new View([
            'status' => true,
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ], Response::HTTP_OK);
    }


    /*
     * Register user
     *
     * @SWG\Tag(name="Security")
     * @Route("/api/register", methods={"POST"})
     *
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     required=true,
     *     @SWG\Schema(
     *          @SWG\Property(
     *              property="email",
     *              type="string",
     *              minimum=6,
     *              maximum=64
     *          ),
     *          @SWG\Property(
     *              property="username",
     *              type="string",
     *              minimum=4,
     *              maximum=64
     *          ),
     *          @SWG\Property(
     *              property="phone",
     *              type="string",
     *              minimum=6,
     *              maximum=64
     *          ),
     *          @SWG\Property(
     *              property="password",
     *              type="string",
     *              minimum=6,
     *              maximum=64
     *          )
     *     )
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Register user",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"})
     * @param Request $request
     * @return View
     */
    public function register(Request $request)
    {
        try {
            /** @var User $user */
            $user = $this->get(FormHandler::class)
                ->handle(RegisterFormType::class, new User(), $request->request->all());
        } catch (InvalidFormException $e) {
            return new View([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => $e->getMessage()
            ], Response::HTTP_OK);
        }

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        $user->setEnabled(true);
        $user->addRole('ROLE_OPERATOR');
        $user->setHash($user->getUsername());
        $user->setPlainPassword($request->get('password'));
        $userManager->updateUser($user);

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'hash' => $user->getHash(),
                'exp' => time() + 43200
            ]);

        return new View([
            'status' => true,
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ], Response::HTTP_OK);
    }

    /**
     * Logout user
     *
     * @SWG\Tag(name="Security")
     * @Route("/api/logout", methods={"DELETE"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Logout user",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     *
     * @return View
     */
    public function logout()
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $this->getUser();
        $user->setHash($user->getUsername());
        $userManager->updateUser($user);
        return new View([
            'status' => true,
            'data' => ['message' => 'User successfully logout!']
        ], Response::HTTP_OK);
    }
}
