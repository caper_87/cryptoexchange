<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Form\ExchangerFormType;
use App\Form\FormHandler;
use App\Managers\ExchangerManager;
use App\Repository\ExchangerRepository;
use App\Traits\HelperTrait;
use App\Traits\PaginationTrait;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Exchanger;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;

class ExchangerController extends FOSRestController
{
    use PaginationTrait, HelperTrait;

    /**
     * @var ExchangerRepository
     */
    public $repository;

    public function __construct(ExchangerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Find all Exchangers
     *
     * @SWG\Tag(name="Exchangers")
     * @Route("/api/exchangers/{page}/{maxItems}", defaults={"page"=1, "maxItems"=10}, methods={"GET"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *      in="query",
     *      name="sort",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Parameter(
     *      in="query",
     *      name="direction",
     *      required=false,
     *      type="string"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Find all Exchangers ",
     *     @SWG\Schema(
     *         type="collection",
     *         @SWG\Items(ref=@Model(type=Exchanger::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param  $page
     * @param $maxItems
     * @param Request $request
     * @return View
     */
    public function index($page, $maxItems, Request $request)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $exchangers = $this->repository->findAllExchangers(
                $request->query->get('sort') ?? $this->defaultSort ,
                $request->query->get('direction') ?? $this->defaultDirection
            );
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($this->paginate($exchangers, $page, $maxItems));
    }

    /**
     * Find all available Exchangers
     *
     * @SWG\Get(
     *    path="/api/exchangers/available/",
     *    tags={"Exchangers"},
     *    @SWG\Parameter(
     *        in="query",
     *        name="admin",
     *        required=false,
     *        type="boolean"
     *    ),
     *    @SWG\Parameter(
     *         in="query",
     *         name="sort",
     *         required=false,
     *         type="string"
     *    ),
     *    @SWG\Parameter(
     *         in="query",
     *         name="direction",
     *         required=false,
     *         type="string"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Find all available Exchangers ",
     *        @SWG\Schema(
     *            type="collection",
     *            @SWG\Items(ref=@Model(type=Exchanger::class, groups={"full"}))
     *        )
     *    )
     * )
     *
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/exchangers/available/", methods={"GET"})
     * @param Request $request
     * @return View
     */
    public function indexAvailable(Request $request)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $admin = $request->query->getBoolean('admin');
            $exchangers = $this->repository->findAllAvailable(
                $admin,
                $request->query->get('sort') ?? $this->defaultSort ,
                $request->query->get('direction') ?? $this->defaultDirection
            );
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess($exchangers);
    }

    /**
     * Find Exchanger by ID
     *
     * @SWG\Tag(name="Exchangers")
     * @Route("/api/exchanger/{id}", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Find Exchanger by ID",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Exchanger::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param $id
     * @return View
     */
    public function show($id)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            if (!$exchanger = $this->repository->find($id)) {
                throw new NotFoundHttpException(null, null, Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$exchanger]);
    }

    /**
     * Check is transaction available
     *
     * @SWG\Tag(name="Exchangers")
     * @Route("/api/exchanger/{id}/transaction/available", methods={"GET"})
     * @Security(name="Bearer")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Check is transaction available",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Exchanger::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param $id
     * @return View
     */
    public function checkIsTransactionAvailable($id)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            if (!$exchanger = $this->repository->find($id)) {
                throw new NotFoundHttpException(null, null, Response::HTTP_NOT_FOUND);
            }
            if (!$exchanger->getAdmin() || !$exchanger->getOperator()) {
                throw new BadRequestHttpException($this->get('translator')->trans('exchanger.transaction.not_available'));
            }
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess([$exchanger]);
    }

    /**
     * Create Exchanger
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/exchanger",
     *    tags={"Exchangers"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="country",
     *        required=true,
     *        type="string",
     *        minimum=4,
     *        maximum=100
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="city",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=100
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="street",
     *        required=true,
     *        type="string",
     *        minimum=2,
     *        maximum=1000
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="phone",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="ip",
     *        required=true,
     *        type="string"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Create exchanger",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Exchanger::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/exchanger", methods={"POST"})
     * @param Request $request
     * @param ExchangerManager $manager
     * @return View
     */
    public function create(Request $request, ExchangerManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                ->handle(ExchangerFormType::class, new Exchanger(), $request->request->all());
            $exchanger = $manager->saveFromForm($form);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }

        return $this->returnSuccess([$exchanger]);
    }

    /**
     * Update Exchanger
     *
     * @SWG\Put(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/exchanger/{id}/update",
     *    tags={"Exchangers"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="country",
     *        required=false,
     *        type="string",
     *        minimum=4,
     *        maximum=100
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="city",
     *        required=false,
     *        type="string",
     *        minimum=6,
     *        maximum=100
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="street",
     *        required=false,
     *        type="string",
     *        minimum=2,
     *        maximum=1000
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="phone",
     *        required=false,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="ip",
     *        required=false,
     *        type="string"
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Update exchanger",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=Exchanger::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @Route("/api/exchanger/{id}/update", methods={"PUT"})
     * @param Exchanger $exchanger
     * @param Request $request
     * @param ExchangerManager $manager
     * @return View
     */
    public function update(Exchanger $exchanger, Request $request, ExchangerManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $form = $this->get(FormHandler::class)
                ->handle(ExchangerFormType::class, $exchanger, $request->request->all(), [], false);
            $exchanger = $manager->saveFromForm($form);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }

        return $this->returnSuccess([$exchanger]);
    }

    /**
     * Disable Exchanger
     *
     * @SWG\Tag(name="Exchangers")
     * @Route("/api/exchanger/{id}/disable", methods={"DELETE"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *    in="path",
     *    name="id",
     *    required=true,
     *    type="integer"
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="Disable Exchanger",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=Exchanger::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param Exchanger $exchanger
     * @param ExchangerManager $manager
     * @return View
     */
    public function disable(Exchanger $exchanger, ExchangerManager $manager)
    {
        try {
            $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN, null, $this->get('translator')->trans('security.access_denied'));
            $manager->disable($exchanger);
        } catch (\Exception $e) {
            return $this->riseError($e);
        }
        return $this->returnSuccess(['message' => $this->get('translator')->trans('exchanger.disable')]);
    }
}
