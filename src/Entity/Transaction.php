<?php

namespace App\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 * @ORM\Table(name="app_transaction")
 */
class Transaction
{
    const STATUS_NEW = 1;
    const STATUS_DECLINE = 2;
    const STATUS_DONE = 3;

    const TYPE_BUY = 1;
    const TYPE_SELL = 2;

    const AVAILABLE_STATUSES = [
        self::STATUS_NEW,
        self::STATUS_DECLINE,
        self::STATUS_DONE
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(type="float")
     */
    private $income;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(type="float")
     */
    private $outcome;

    /**
     * @Assert\Regex(pattern="/(1|2)/")
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(type="float")
     */
    private $course;

    /**
     * @Assert\Regex(pattern="/[+-]?([0-9]*[.])?[0-9]+/")
     * @ORM\Column(type="float", nullable=true)
     */
    private $fee;

    /**
     * @Assert\Length(min=6)
     * @Assert\Length(max=64)
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $courseProvider;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", inversedBy="transactions")
     */
    private $currency;

    /**
     * @Assert\Length(min=6)
     * @Assert\Length(max=1000)
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $purse;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Purse", inversedBy="transactions")
     */
    private $purseOwn;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Exchanger", inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $exchanger;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="operatorTransactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $operator;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="adminTransactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $admin;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dialogs\Dialog", mappedBy="transaction")
     */
    protected $dialog;


    /**
     *
     * GETTERS AND SETTERS
     *
     */


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float|null
     */
    public function getIncome(): ?float
    {
        return $this->income;
    }

    /**
     * @param float $income
     * @return Transaction
     */
    public function setIncome(float $income): self
    {
        $this->income = $income;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getOutcome(): ?float
    {
        return $this->outcome;
    }

    /**
     * @param float $outcome
     * @return Transaction
     */
    public function setOutcome(float $outcome): self
    {
        $this->outcome = $outcome;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return float|null
     */
    public function getCourse(): ?float
    {
        return $this->course;
    }

    /**
     * @param float $course
     * @return Transaction
     */
    public function setCourse(float $course): self
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Transaction
     */
    public function setCurrency($currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPurse(): ?string
    {
        return $this->purse;
    }

    /**
     * @param string $purse
     * @return Transaction
     */
    public function setPurse(string $purse): self
    {
        $this->purse = $purse;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Transaction
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getFinishedAt(): ?\DateTimeInterface
    {
        return $this->finishedAt;
    }

    /**
     * @param \DateTimeInterface|null $finishedAt
     * @return Transaction
     */
    public function setFinishedAt(?\DateTimeInterface $finishedAt): self
    {
        $this->finishedAt = $finishedAt;

        return $this;
    }

    /**
     * @return Exchanger|null
     */
    public function getExchanger(): ?Exchanger
    {
        return $this->exchanger;
    }

    /**
     * @param $exchanger
     * @return Transaction
     */
    public function setExchanger($exchanger): self
    {
        $this->exchanger = $exchanger;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getOperator(): ?User
    {
        return $this->operator;
    }

    /**
     * @param $operator
     * @return Transaction
     */
    public function setOperator($operator): self
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return float|null
     */
    public function getFee(): ?float
    {
        return $this->fee;
    }

    /**
     * @param float|null $fee
     * @return Transaction
     */
    public function setFee(?float $fee): self
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCourseProvider(): ?string
    {
        return $this->courseProvider;
    }

    /**
     * @param null|string $courseProvide
     * @return Transaction
     */
    public function setCourseProvider(?string $courseProvide): self
    {
        $this->courseProvider = $courseProvide;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPurseOwn()
    {
        return $this->purseOwn;
    }

    /**
     * @param null|string $purseOwn
     * @return Transaction
     */
    public function setPurseOwn($purseOwn): self
    {
        $this->purseOwn = $purseOwn;

        return $this;
    }
}
