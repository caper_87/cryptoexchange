<?php

namespace App\Entity\Dialogs;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_dialog")
 */
class Dialog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="dialog")
     */
    private $transaction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dialogs\DialogUser", mappedBy="dialog")
     */
    private $dialogUser;
}