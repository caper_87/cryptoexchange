<?php

namespace App\Entity\Dialogs;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_dialog_user")
 */
class DialogUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Dialogs\Dialog", inversedBy="dialogUser")
     */
    private $dialog;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userDialog")
     */
    private $user;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lastMessageId;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $dateCreate;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $dateEnd;

}