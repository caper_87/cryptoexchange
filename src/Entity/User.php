<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="app_user")
 */
class User extends BaseUser
{
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_OPERATOR = 'ROLE_OPERATOR';

    const MAIN_ROLES = [
        self::ROLE_SUPER_ADMIN,
        self::ROLE_ADMIN,
        self::ROLE_OPERATOR
    ];

    const DEFAULT_ADMIN_PERMISSIONS = [];
    const DEFAULT_OPERATOR_PERMISSIONS = [
        'ROLE_CREATE_TRANSACTION'
    ];

    const ALLOWED_PERMISSIONS = [
        'ROLE_MANAGE_PURSES',
        'ROLE_MANAGE_FEE',
        'ROLE_WATCHER',
        'ROLE_CREATE_TRANSACTION',
        'ROLE_VIEW_TRANSACTION',
        'ROLE_MANAGE_CURRENCIES',
        'ROLE_UPDATE_TRANSACTION',

        'ROLE_VIEW_ADMIN',
        'ROLE_CREATE_ADMIN',
        'ROLE_UPDATE_ADMIN',
        'ROLE_DELETE_ADMIN',
        'ROLE_VIEW_OPERATOR',
        'ROLE_CREATE_OPERATOR',
        'ROLE_UPDATE_OPERATOR',
        'ROLE_DELETE_OPERATOR',
        'ROLE_VIEW_EXCHANGER',
        'ROLE_CREATE_EXCHANGER',
        'ROLE_UPDATE_EXCHANGER',
        'ROLE_DELETE_EXCHANGER',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     * @Assert\Length(max=64)
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     * @Assert\Email
     *
     * @var string
     */
    protected $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     * @Assert\Length(max=64)
     * @var string
     */
    protected $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     * @Assert\Length(max=64)
     * @ORM\Column(type="string", nullable=true)
     */
    protected $firstName;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     * @Assert\Length(max=64)
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lastName;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     * @var
     */
    protected $lastLogin;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $hash;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="admin")
     */
    protected $adminTransactions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Exchanger", mappedBy="admin")
     */
    protected $adminExchangers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="operator")
     */
    protected $operatorTransactions;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Exchanger", mappedBy="operator")
     */
    protected $operatorExchanger;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dialogs\DialogUser", mappedBy="user")
     */
    protected $userDialog;

    /**
     *
     * GETTERS AND SETTERS
     *
     */


    public function __construct()
    {
        parent::__construct();
        $this->adminTransactions = new ArrayCollection();
        $this->adminExchangers = new ArrayCollection();
        $this->operatorTransactions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getOperatorExchanger()
    {
        return $this->operatorExchanger;
    }

    /**
     * @param mixed $operatorExchanger
     */
    public function setOperatorExchanger($operatorExchanger): void
    {
        $this->operatorExchanger = $operatorExchanger;
    }

    /**
     * @return object
     */
    public function getModelForApi()
    {
        return (object) [
            'id'         => $this->getId(),
            'username'   => $this->getUsername(),
            'email'      => $this->getEmail(),
            'role'       => $this->getMainRole(),
            'first_name' => $this->getFirstName(),
            'last_name'  => $this->getLastName(),
            'phone'      => $this->getPhone(),
            'last_login' => $this->getLastLogin()->format('Y-m-d H:i:s'),
            'created_at' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
            'updated_at' => $this->getUpdatedAt()->format('Y-m-d H:i:s'),
            'enabled'    => $this->getEnabled(),
            'permissions'=> $this->getPermissions(),
        ];
    }

    public function getMainRole()
    {
        if ($this->hasRole('ROLE_SUPER_ADMIN')) return 'super-admin';
        if ($this->hasRole('ROLE_ADMIN')) return 'admin';
        if ($this->hasRole('ROLE_OPERATOR')) return 'operator';
    }

    public function getPermissions()
    {
        return array_values(array_diff($this->getRoles(), self::MAIN_ROLES));
    }

    public function setFakeEmail()
    {
        $this->email = md5(rand(1,100)).'@'.md5(rand(1,100)).'.com';
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = crypt($hash, rand(1, 1000));
    }

    /**
     * @return ArrayCollection
     */
    public function getAdminExchangers()
    {
        return $this->adminExchangers;
    }

    /**
     * @param Exchanger $exchanger
     * @return $this
     */
    public function addAdminExchangers(Exchanger $exchanger)
    {
        $this->adminExchangers[] = $exchanger;
        return $this;
    }

    /**
     * @param Exchanger $exchanger
     */
    public function removeAdminExchangers(Exchanger $exchanger)
    {
        $this->adminExchangers->removeElement($exchanger);
    }


    /**
     * @return ArrayCollection
     */
    public function getAdminTransactions()
    {
        return $this->adminTransactions;
    }

    /**
     * @param Transaction $transaction
     * @return $this
     */
    public function addAdminTransactions(Transaction $transaction)
    {
        $this->adminTransactions[] = $transaction;
        return $this;
    }

    /**
     * @param Transaction $transaction
     */
    public function removeAdminTransactions(Transaction $transaction)
    {
        $this->adminTransactions->removeElement($transaction);
    }

    /**
     * @return ArrayCollection
     */
    public function getOperatorTransactions()
    {
        return $this->operatorTransactions;
    }

    /**
     * @param Transaction $transaction
     * @return $this
     */
    public function addOperatorTransactions(Transaction $transaction)
    {
        $this->operatorTransactions[] = $transaction;
        return $this;
    }

    /**
     * @param Transaction $transaction
     */
    public function removeOperatorTransactions(Transaction $transaction)
    {
        $this->operatorTransactions->removeElement($transaction);
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return null|string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param null|string $confirmationToken
     * @return void
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return void
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @param \DateTime|null $lastLogin
     * @return void
     */
    public function setLastLogin(\DateTime $lastLogin = null)
    {
        $this->lastLogin = $lastLogin;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return void
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }
}
