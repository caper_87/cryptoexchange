<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeeRepository")
 * @ORM\Table(name="app_fee")
 */
class Fee
{

    const INCREASE = 1;
    const DECREASE = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $currencyIn;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $currencyOut;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $percent;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/(1|2)/")
     * @ORM\Column(type="integer")
     */
    private $direction;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;


    /**
     *
     * GETTERS AND SETTERS
     *
     */


    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCurrencyIn()
    {
        return $this->currencyIn;
    }

    /**
     * @param mixed $currencyIn
     */
    public function setCurrencyIn($currencyIn): void
    {
        $this->currencyIn = $currencyIn;
    }

    /**
     * @return mixed
     */
    public function getCurrencyOut()
    {
        return $this->currencyOut;
    }

    /**
     * @param mixed $currencyOut
     */
    public function setCurrencyOut($currencyOut): void
    {
        $this->currencyOut = $currencyOut;
    }

    /**
     * @return mixed
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param mixed $percent
     */
    public function setPercent($percent): void
    {
        $this->percent = $percent;
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param mixed $direction
     */
    public function setDirection($direction): void
    {
        $this->direction = $direction;
    }
}
