<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExchangerRepository")
 * @ORM\Table(name="app_exchanger")
 */
class Exchanger
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     * @Assert\Length(max=100)
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     * @Assert\Length(max=100)
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     * @Assert\Length(max=1000)
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     * @Assert\Length(max=64)
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/")
     */
    private $ip;

    /**
     *  @ORM\Column(type="boolean", options={"default" : true})
     */
    private $enabled;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="exchanger")
     */
    protected $transactions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="adminExchangers")
     * @ORM\JoinColumn(name="admin_id", referencedColumnName="id", nullable=true)
     */
    protected $admin;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="operatorExchanger")
     * @ORM\JoinColumn(name="operator_id", referencedColumnName="id", nullable=true)
     */
    protected $operator;


    /**
     *
     * GETTERS AND SETTERS
     *
     */


    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $enabled
     */
    public function setEnabled($enabled): void
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param mixed $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return ArrayCollection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @param Transaction $transaction
     * @return $this
     */
    public function addTransactions(Transaction $transaction)
    {
        $this->transactions[] = $transaction;
        return $this;
    }

    /**
     * @param Transaction $transaction
     */
    public function removeTransactions(Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param $admin
     * @return Exchanger
     */
    public function setAdmin($admin): self
    {
        $this->admin = $admin;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }
}
